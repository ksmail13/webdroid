/* messageCollection is created by VODEK,
  collection of messages used in alert
 */
  
var signinTitle="Sign in";
var signupTitle="Sign up";
var serverTitle="Server";
var profileTitle="Profile";
var fileupTitle="File Upload";
var pwTitle="Password";
var pwresetTitle="Reset password";
var gitTitle="GitId";
var memberTitle="Unsubscribe";
var uploadTitle="Upload";
var is_enableTitle="Sign in again";
var imageTitle="Image upload";


var SC="success";
var profileSCmsg="Profile is successfully updated!";
var fileupSCmsg="File is uploaded successfully!";
var signupSCmsg="Signed up successfully! <br> Enjoy WEBDROID after sign-in.";
var gitSCmsg="GitID is updated successfully!";
var pwchSCmsg="Password is changed successfully!";
var memberSCmsg="Your account is successfully unsubscribed. <br> you can rejoin anytime!";
var pwsetSCmsg="Your password is resetted successfully!";
var checkmailSCmg="Please check your email!";
var is_enableSCmg="You unsubscribed before. <br>Do you want to sign in again?";
var imgSCmsg="Image is uploaded successfully!";

var ERR="error";
var serverERRmsg ="Server Error!!<br>We apologize for inconvenience.<br>"
var signinERRmsg="Signin Failed!";
var signupERRmsg="Signup failed.";
var profileERRmsg="Failed to update profile.";
var fileupERRmsg="Failed to upload file.";
var pwmatchERRmsg="password doesn't match";
var gitERRmsg="Failed to update GitID.";
var pwchERRmsg="Failed to change password";
var memberERRmsg="Failed to unsubscribe";
var memberCancelmsg="Cancel to unsubscribe";
var pwsetERRmsg="Failed to reset password";
var imgERRmsg="Failed to upload image.";

var tryAgain="Please try again";
var emptyCheck="Please check if there's any empty space";
var check="Please check it";
var fillin="You should fill in ";
var memberFinal= 'Do you really want to unsubscribe?';
var wait="please wait";



// ----------------------------------- UI text begin ---------------------------- //
var ok = "Ok";
var cancel = "Cancel";

// ----------------------------------- UI text end ---------------------------- //