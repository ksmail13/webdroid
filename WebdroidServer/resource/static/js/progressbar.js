//progressbar.js is created by VODEK.
$(document).ready(function() {

  var $progressBar = $('.progress-bar');
  var $progressModal = $('#dv-uploadProgress');
  var $statTxt = $('.txt');
  var $closeBtn = $('.modal-close');
  var $uploader = $('#uploader');
  var $fileupModal=$('#fileupModal');
  var buildFailed = 0;

  var isUploading = false;
  $('#upload').click(function(){
    if(!isUploading) {
      //var fileExtPoint = $uploader.val().lastIndexOf('.');
      //var fileExt = $uploader.val().substring(fileExtPoint+1);


      //if(fileExt == 'zip') {
        $('#fileup').submit();
        $fileupModal.modal('hide');
        $progressModal.modal('show');

   
    } else {
      $progressModal.modal('show');
    }
  });

  $('#fileup').ajaxForm({
      beforeSend: function() {
          var percentVal = '0%';
          $progressBar.width(percentVal);
          $progressBar.html(percentVal);
          $statTxt.html(' ');
          $closeBtn.attr('disabled', 'disabled');
          $closeBtn.html(wait);
          isUploading = true;
      },
      uploadProgress: function(event, position, total, percentComplete) {
          //var percentVal = percentComplete + '%';
          //$progressBar.css('width', percentVal);
          //$progressBar.html(percentVal);
      },
      success: function() {
          //var percentVal = '100%';
          //$progressBar.width(percentVal);
          //$progressBar.html(percentVal);
      },
      complete: function(xhr) {
        isUploading = false;
      }
  });
  $closeBtn.click(function(){
    if(!isUploading) location.reload();
  });
  $('input#upload').click(function(){
      $progressModal.modal({
        backdrop: 'static',
        keyboard: false
      });
      var sock = new SockJS('/fileup');
      buildFailed = 0;
      sock.onopen = function() {
        //sock.send('run');
        console.log('pb sock open');
      };
      sock.onclose = function() {
        //sock.send('sock close');
        console.log('pb sock close');
      };
      sock.onmessage = function(e) {
        console.log(e);
        console.log('message recieved', e.data);
        var percentVal = e.data;
        if(percentVal>0 && !buildFailed) $progressBar.css('width', percentVal + '%');
        if(percentVal<0){ // BUILD FAILED
          $progressBar.html("Build Failed");
          $progressBar.width("100%");
          if(!buildFailed) $statTxt.html("Unhandled exception occured");
          $uploader.val("");
          $closeBtn.removeAttr('disabled');
          $closeBtn.html(ok);
          isUploading = false;
          buildFailed = 1;
          sock.close();
          sock = null;
        }
        if(percentVal==35 && !buildFailed) $progressBar.html("Uploading..");
        if(percentVal==45 && !buildFailed) $progressBar.html("Building..");
        if(percentVal==75 && !buildFailed) $progressBar.html("Building..");
        if(percentVal==100 && !buildFailed){
          var percentVal = '100%';
          $progressBar.width(percentVal);
          $progressBar.html("Complete");
          $statTxt.html(fileupSCmsg);
          $uploader.val("");
          $closeBtn.removeAttr('disabled');
          $closeBtn.html(ok);
          isUploading = false;
          sock.close();
          sock = null;
        }
        if(percentVal[0]==0){
          $statTxt.html(e.data.substring(1));
          buildFailed = 1;
        }
      };

  });

});



