
var modelist = ace.require("ace/ext/modelist");
var page_cnt = 0;
var tab_cnt = 0;
var tab_init = false;
var tab_static_width = 175;

$(document).ready( function(){

  var p_id = $("#projectTree").attr("rel");
  var frameBuffer = new Image();

  $("body").layout( {
        defaults: {
              size:                   "auto"
              ,   contentSelector:        ".content"  // inner div to auto-size so only it scrolls, not the entire pane!
              ,   contentIgnoreSelector:  "span"      // 'paneSelector' for content to 'ignore' when measuring room for content
              ,   togglerLength_open:     35          // WIDTH of toggler on north/south edges - HEIGHT on east/west edges
              ,   togglerLength_closed:   35          // "100%" OR -1 = full height
              ,   hideTogglerOnSlide:     false        // hide the toggler when pane is 'slid open'
              //  effect defaults - overridden on some panes
              ,   fxName:                 "slide"     // none, slide, drop, scale
              ,   fxSpeed_open:           750
              ,   fxSpeed_close:          750
              ,   fxSettings_open:        { easing: "easeOutQuint" }
              ,   fxSettings_close:       { easing: "easeOutQuint" }
              }
              ,
              north: {
              spacing_open:           0           // cosmetic spacing
              ,   togglerLength_open:     0           // HIDE the toggler button
              ,   togglerLength_closed:   -1          // "100%" OR -1 = full width of pane
              ,   resizable:              false
              ,   slidable:               false
              ,   fxName:                 "none"
              ,   showOverflowOnHover: true
              }
              ,   west: {
              size:                   .22
              ,   minSize:                .06
              ,   maxSize:                .80
              ,   spacing_closed:         4
              ,   togglerLength_closed:   -1
              ,   resizable:              true
              ,   slidable:               true
        }
  });

  requestAysnc('/treebase','post',{pid : p_id},function(p_path){
        console.log(p_path);
        $('#projectTree').fileTree({ root: p_path }, function(file) {
              requestAysnc('/openfile','post',{filepath: file, pid : p_id}, function(data) {
                  addTab(file,data.innerText,data.isPic);
              }, function(){});
        })
  },function(){});

  $("#playbtn").click( function(){
        var url = '/remotedevice/'+p_id;
        var width = 400;
        var height = 680;
        var top = window.screenY+(window.outerHeight/2)-(height/2);
        var left = window.screenX+(window.outerWidth/2)-(width/2);
        var vmPopup = window.open(url,
          '_blank'
          , 'toolbar=no \
          , location=no \
          , directories=no \
          , status=no \
          , menubar=no \
          , scrollbars=no \
          , resizable=no \
          , copyhistory=no \
          , width='+width+' \
          , height='+height+' \
          , top='+top+' \
          , left='+left);
  });

  $('#favoratebtn').click(function(e){
    e.preventDefault();
    var $this = $(this);
    var spid = $this.attr("pid");
    var isFav = $this.attr("isfav") == "true";
    var requestPath = $this.attr('href');
    

    requestAysnc(requestPath,'get', null ,function(data) {
      if(data.result){
        clearfavoratebtn();
        if(isFav) {
          pluscancelbtn();
        } else {
          plusfavoratebtn();
        }
        $this.attr("isfav", !isFav+'');
      }
      else
        modalAlert("failed", "favorate fail");
    },null);
  });
/*
  $('#cancel-favoratebtn').click(function(){
        var spid = $(this).attr("pid");
        requestAysnc('/cancel_favorate_projectlist?id='+spid,'get', null ,function(data) {
          if(data.result){
            clearcancelbtn();
          }
          else
            modalAlert("failed", "favorate cancel fail");
        },null);
  });
*/
  $(window).resize(resizeBrowser);
  resizeBrowser();
}); //end of document ready function

function resizeBrowser() {
  $('.tab-pane.active').height($(window).height()/100*88);
  $('#projectTree').height($(window).height()/100*88);
  if(tab_cnt*tab_static_width > $(window).width()/100*75){
    var tab_margin = $('.tab').css('margin-right');
    tab_margin = tab_margin.substring(0,tab_margin.length-2);
    $('.tab').css('width',function(){
      return $(window).width()/100*74/tab_cnt - tab_margin;
    });
  }
};

function addTab(rel_path,data,isPic){
  var filename =  rel_path.substring(rel_path.lastIndexOf('/')+1);
  if(!tab_init){ // add tab when tab_cnt == 0
    $('.tab-content').prepend('<div class="tab-pane active" id="page'+(++page_cnt)+'" rel="'+rel_path+'"></div>');
    $('.tabs').prepend('<li class="tab active" data-target="#page'+page_cnt+'"> <span>'+filename+'</span> \
      <button type="button" class="close" onclick="deleteTab('+page_cnt+')">×</button></li>');
    tab_cnt++;
    if(isPic) tabImageSetter(page_cnt,filename,data);
    else tabTextSetter(page_cnt,filename,data);
    $('.tabs').chrometab();
    tab_init = true;
  }
  else if($('.tab-pane[rel="'+rel_path+'"]')[0] != undefined){ // open already opened
    $('.active').removeClass('active');
    $('.tab-pane[rel="'+rel_path+'"]').addClass('active');
    $('.tab[data-target=#'+$('.tab-pane.active').attr('id')+']').addClass('active');
  }
  else{ // add tab
    $('.tab-content').prepend('<div class="tab-pane" id="page'+(++page_cnt)+'" rel="'+rel_path+'"></div>');
    $('.tabs').prepend('<li class="tab" data-target="#page'+page_cnt+'"> <span>'+filename+'</span> \
      <button type="button" class="close" onclick="deleteTab('+page_cnt+')">×</button></li>');
    tab_cnt++;
    $('.active').removeClass('active');
    $('.tab-pane').first().addClass('active');
    $('.tab').first().addClass('active');
    var tab_width = $('.tab').css('width'); // 175px
    tab_width = tab_width.substring(0,tab_width.length-2);
    tab_width *= 1; //string to num
    if(tab_cnt*tab_width > $(window).width()/100*75){
      var tab_margin = $('.tab').css('margin-right');
      tab_margin = tab_margin.substring(0,tab_margin.length-2);
      var x = $(window).width()/100*74/tab_cnt - tab_margin;
      $('.tab').css('width',x);
    }
    if(isPic) tabImageSetter(page_cnt,filename,data);
    else tabTextSetter(page_cnt,filename,data);
    $('.tabs').chrometab();
  }
}

function deleteTab(target){
  if(--tab_cnt == 0) tab_init = false;
  var tab_width = $('.tab').css('width');
  tab_width = tab_width.substring(0,tab_width.length-2);
  tab_width *= 1; //string to num
  if(tab_width < tab_static_width){
    var tab_margin = $('.tab').css('margin-right');
    tab_margin = tab_margin.substring(0,tab_margin.length-2);
    $('.tab').css('width',function(){
      var x = $(window).width()/100*74/tab_cnt - tab_margin;
      if(x >= tab_static_width) return tab_static_width; 
      else return x;
    });
  }
  $('.active').removeClass('active');
  if($('.tab-pane#page'+target).index('.tab-pane') == 0){
    $('.tab-pane#page'+target).next().addClass('active');
    $('.tab[data-target=#page'+target+']').next().addClass('active');
  }else{
    $('.tab-pane#page'+target).prev().addClass('active');
    $('.tab[data-target=#page'+target+']').prev().addClass('active');
  }
  $('.tab-pane#page'+target).remove();
  $('.tab[data-target=#page'+target+']').remove();
  $('.tabs').chrometab();
}

function tabTextSetter(page_cnt,filename,data){
  var editor = ace.edit("page"+page_cnt);
  editor.setTheme("ace/theme/monokai");
  editor.getSession().setMode("ace/mode/text");
  editor.setReadOnly(true);
  editor.getSession().setMode(modelist.getModeForPath(filename).mode);  
  editor.setValue(data);
  editor.gotoLine(1);
  $('.tab-pane.active').height($(window).height()/100*80);
}

function tabImageSetter(page_cnt,filename,data){
  var ext = filename.substring(filename.lastIndexOf(".")+1);
  var img = new Image();
  img.src = 'data:image/'+ext+';base64,' + data;
  $('#page'+page_cnt).prepend('<canvas id="canvas'+page_cnt+'" width="'+img.width+'px" height="'+img.height+'px"></canvas><div class="picName">'+filename+'</div>');
  $('#page'+page_cnt).css('text-align','center');
  var ctx = document.getElementById('canvas'+page_cnt).getContext('2d');
  ctx.drawImage(img,0,0);
  $('.tab-pane.active').height($(window).height()/100*80);
}

function clearcancelbtn() {
  $('#cancel-favoratebtn').html('');
}

function clearfavoratebtn() {
   $('#favoratebtn').html('');
}

function emptybtn() {
  return '<span class="glyphicon glyphicon-star-empty"></span> Bookmark';
}

function fullbtn() {
  return '<span class="glyphicon glyphicon-star"></span> Bookmark';
}

function pluscancelbtn() {
  var myHTML='';
  myHTML+=emptybtn();
  $('#favoratebtn').html(myHTML);
}

function plusfavoratebtn() {
  var myHTML='';
  myHTML+=fullbtn();
  $('#favoratebtn').html(myHTML);

}








