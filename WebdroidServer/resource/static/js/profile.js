// profile.js is created by VODEK. 

$(document).ready(function () {
   $('#proimg').imageUploader('/p_img_upload'); //Upload profile image
   
   $('#frm-update-introduce').submit(function () {
  
     return formRequest('#frm-update-introduce',
       function (data) {
         if (data.result) {
           modalAlert(profileTitle, profileSCmsg, function () {
             location.reload();
           });
           $('.modal-back').remove();
         } else {
           modalAlert(profileTitle, profileERRmsg + '<br>' + tryAgain, function () {});
         }
       },
       function (error) {
         modalAlert(serverTitle, serverERRmsg + '<br>' + tryAgain, function () {});
       }
     );
   });
 });