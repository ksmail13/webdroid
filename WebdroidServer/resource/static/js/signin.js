/*signin.js is created by VODEK.
  Javascript for welcome.jade, signin_original.jade
*/

$(document).ready(function () {
  var isSavedId = getCookie('saveId');
  var savedId  = getCookie('id');
  if(isSavedId) {
      $('#chk-save-id').attr('checked', true);
      $('#txt-user-id').val(savedId);
}

$('#frm-signin').submit(function (e) {
//alert();
  if($('#chk-save-id').is(':checked')) {
      setCookie('saveId', 'true', 10000000);
      setCookie('id', $('#txt-user-id').val(), 10000000);
  }
  return formRequest(this, onReqSuccess, onReqFailed);
  });
});
function onReqSuccess(data) {
  if(data.result) {
    if(data.is_enable==false){
      modalConfirm(is_enableTitle,is_enableSCmg,
                 function(){requestAysnc('/enable','post',{'user_id':$('#txt-user-id').val()},function(){location.href='/projectmain'},
                                          function(){});},
                   function(){}
      );
    }
    else{
      location.replace('/projectmain');
    }
  } else {
      modalAlert(signinTitle+" " +ERR, signinERRmsg+'<br>'+tryAgain);
  }
}

function onReqFailed(data) {
  var res = $.parseJSON(data.responseText);
   modalAlert(signinTitle, signinERRmsg+'<br>'+tryAgain);
}

