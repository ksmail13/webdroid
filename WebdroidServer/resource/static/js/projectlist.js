//projectlist.js is created by VODEK.
//make projectBlock

function projectBlock(id, name, description, isFavorite, path) {
  var star = isFavorite?'star':'star-empty';
  var blockType = isFavorite?'favorate-block':'project-block';
  
  
  return '<div class="col-sm-6 col-md-4 text-center">'+
            '<div class="thumbnail block '+blockType+'" pid="'+id+'" >'+
              '<div class="row">'+
                '<div class="col-xs-12">'+
                  '<img class=" project-image" src="">'+//projimg/'+id+'">'+
                '</div>'+
              '</div>'+
              '<div class="row">'+
                '<div class="col-xs-12">'+
                  '<div class="row">'+
                    '<div class="col-xs-12">'+
                      '<h4>'+name+'</h4>'+
                    '</div>'+
                  '</div>'+
                  '<div class="row">'+
                    '<div class="col-xs-12">'+
                      '<h4><small>'+description+'</small></h4>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>'+
              '<div class="row">'+
                '<div class="col-xs-12 text-center">'+
                  '<div class="btn-group btn-group-xs">'+
                    '<a class="btn btn-default deletebutton" pid="'+id+'" href="#"><span class="glyphicon glyphicon-minus"></span></a>'+
                    '<a class="btn btn-default starbutton" pid="'+id+'" href="#"><span class="glyphicon glyphicon-'+star+'"></span></a></div>'+
                '</div>'+
              '</div>'+
            '</div>'+
          '</div>';
  
}

$(document).ready(function loadProject(){

    requestAysnc('/show_projectlist','post',null,function(data){
        clearList();
      
        var myHTML = '';
      // project list
        for (var i = 0; i < data.projects.length; i++) {
          var project = data.projects[i];
          myHTML += projectBlock(project.id, project.name, project.description, false, project.path);
        }
        
        $('#dv-projectlist').html(myHTML);

        $('.project-block .deletebutton').click(function(e){
          var $this = $(this);
          var spid = $(this).attr("pid");
          e.stopPropagation();
          requestAysnc('/delete_projectlist','post',{id:spid},function (data){
              loadProject();
          },null);
          e.preventDefault();
        });

        $('.project-block .starbutton').click(function(e){
            var spid = $(this).attr("pid");
            e.stopPropagation();
            requestAysnc('/favorate_projectlist/'+spid,'get',null,function(data) {
                loadProject();
            },null);
            e.preventDefault();
        });

      // favorite list
        myHTML = '';
        for (var i = 0; i < data.favorates.length; i++) {
          var project = data.favorates[i];
          myHTML += projectBlock(project.id, project.name, project.description, true, project.path);
        }
      
        $('#dv-favorate').html(myHTML);

        $('.favorate-block .deletebutton').click(function(e){
            var spid = $(this).attr("pid");
            e.stopPropagation();
            requestAysnc('/delete_projectlist','post',{id:spid},function (data){
                //location.reload();
              
              loadProject();
            },null);
        });

        $('.favorate-block .starbutton').click(function(e){
            var spid = $(this).attr("pid");
            var $this = $(this);
            e.stopPropagation();
            requestAysnc('/favorate_projectlist/'+spid,'get',null,function(data) {
              loadProject();
            },null);
        });
        
        $('.block').click(function(e){
          var spid = $(this).attr("pid");
          location.href="/projectview/"+spid;
        });

    },null);
});

function clearList() {
  $('#dv-favorate').html('');
  $('#dv-projectlist').html('');
}