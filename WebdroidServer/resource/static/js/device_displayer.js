var displayer = null;
var context = null;
var img = new Image();
var buf = "";
var dragging = false;
var sock = new SockJS('/vmeventcontrol');
var fbSock = null;

//-------------------------------socket function
sock.onopen = function () {
  console.log('sock open');
  send('apk#'+pid+'_'+pname);
};

sock.onclose = function () {
  send('sock close');
  console.log('sock close');
  fbdisable();
};

function send(message) {
  if (sock.readyState === SockJS.OPEN) {
    console.log("sending message");
    sock.send(message);
  } else {
    console.log("The socket is not open.");
  }
}

sock.onmessage = function (e) {
  console.log('message recieved', e.data);
  if (e.data == 'run_success') {
    fbenable();
  }
};

function fbenable() {
  $('.three-quarters-loader').css('display','none');
  $('.loadmsg').css('display','none');
  $('#displayer').css('display','block');
  
  if(console) console.log("get framebuffers");
  fbSock = new SockJS('/getFrameBuffer');
  var buffer = '';
  
  var cnt = -1;   // frame buffer state init
  fbSock.onopen = function () {
    console.log("open");
  };
  fbSock.onmessage = function (e) {
    if(e.data == "start") {
      fbSock.send('start'); 
      return ;
    }
    if (cnt == -1) {
      cnt = Number(e.data);
      cnt = Math.ceil(cnt / 4096);
      //console.log("frame size "+cnt);
      if (!isNaN(cnt))
        fbSock.send("ok");
      else
        cnt = -1;
    } else {
      var temp = e.data;
      cnt--;
      buffer += temp;
    }
    //console.log(cnt+"/"+count++);

    if (cnt <= 0) {
      //console.log("receive all frame");
      cnt = -1;
      var img = new Image();
      img.src = "data:image/jpeg;base64," + buffer;
      img.onload = function () {
        context.drawImage(img, 0, 0);
      }
      buffer = '';
      fbSock.send("start");
    }
  };
  fbSock.onclose = function () {
    console.log("framebuffer close");
    cnt = -1;
  };
}

function fbdisable() {
  fbSock.close();
  fbSock = null;
  //fbSock = new SockJS('/getFrameBuffer');
}

//----------------------------------------function
function windowToCanvas(displayer, x, y) {
  var bbox = displayer.getBoundingClientRect();
  return {
    x: x - bbox.left * (displayer.width / bbox.width),
    y: y - bbox.top * (displayer.height / bbox.height)
  };
}

function updateXY(x, y) {
  buf = zeroPad(x.toFixed(0), 100) + '#' + zeroPad(y.toFixed(0), 100);
}

function zeroPad(nr, base) {
  var len = (String(base).length - String(nr).length) + 1;
  return len > 0 ? new Array(len).join('0') + nr : nr;
}

//-------------------------------event handler
function displayerInit(displayer) {
  displayer.addEventListener("keydown", onkeydown, false);
  displayer.addEventListener("keyup", onkeyup, false);
  
  displayer.onmousemove = function (e) {
    var loc = windowToCanvas(displayer, e.clientX, e.clientY);

    e.preventDefault();

    if (dragging == true) {
      updateXY(loc.x, loc.y);
      send('touch#' + buf + '#2');
    }
  };

  displayer.onmousedown = function (e) {
    displayer.focus();
    var loc = windowToCanvas(displayer, e.clientX, e.clientY);

    e.preventDefault();

    dragging = true;
    updateXY(loc.x, loc.y);
    send('touch#' + buf + '#0');
  };

  displayer.onmouseup = function (e) {
    send('touch#' + buf + '#1');
    dragging = false;
  };

  displayer.onmouseout = function (e) {
    if (dragging) send('touch#' + buf + '#1');
    dragging = false;
  };

  function onkeydown(e) {
    var x = (e.which) ? e.which : e.keyCode;
    console.log(x);
    console.log(map.get(x));
    if (x = map.get(x)) send('key#' + x + '#0');
  }

  function onkeyup(e) {
    var x = (e.which) ? e.which : e.keyCode;
    console.log(x);
    console.log(map.get(x));
    if (x = map.get(x)) send('key#' + x + '#1');
  }
}

//----------------------------- initializing
function makeDevice(target) {
  
  var html = "<div class='device_wrapper'><div class='three-quarters-loader'></div><div class='loadmsg'><font color='#B2B2FF' size='5'><center><i>now loading....</i></center></font></div>";
  html += "<canvas id='displayer' width='360' height='640' tabindex='1'> \
		Canvas not supported \
		</canvas></div>";

  $(target).html(html);

  displayer = document.getElementById('displayer');
  context = displayer.getContext('2d');

  displayerInit(displayer);
  $('#displayer').css('display','none');

}
