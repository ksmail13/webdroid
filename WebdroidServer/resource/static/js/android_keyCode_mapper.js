Map = function(){
 this.map = new Object();
};   
Map.prototype = {   
    put : function(key, value){   
        this.map[key] = value;
    },   
    get : function(key){   
        return this.map[key];
    },
    containsKey : function(key){    
     return key in this.map;
    },
    containsValue : function(value){    
     for(var prop in this.map){
      if(this.map[prop] == value) return true;
     }
     return false;
    },
    isEmpty : function(key){    
     return (this.size() == 0);
    },
    clear : function(){   
     for(var prop in this.map){
      delete this.map[prop];
     }
    },
    remove : function(key){    
     delete this.map[key];
    },
    keys : function(){   
        var keys = new Array();   
        for(var prop in this.map){   
            keys.push(prop);
        }   
        return keys;
    },
    values : function(){   
     var values = new Array();   
        for(var prop in this.map){   
         values.push(this.map[prop]);
        }   
        return values;
    },
    size : function(){
      var count = 0;
      for (var prop in this.map) {
        count++;
      }
      return count;
    }
};

var map = new Map();
map.put(8, "KEYCODE_DEL");
map.put(9, "KEYCODE_TAB");
map.put(13, "KEYCODE_ENTER");
map.put(16, "KEYCODE_SHIFT_LEFT");
map.put(17, "KEYCODE_CTRL_LEFT");
map.put(18, "KEYCODE_ALT_LEFT");
map.put(20, "KEYCODE_CAPS_LOCK");
map.put(27, "KEYCODE_ESCAPE");
map.put(32, "KEYCODE_SPACE");
map.put(33, "KEYCODE_PAGE_UP");
map.put(34, "KEYCODE_PAGE_DOWN");
map.put(35, "KEYCODE_ENDCALL");
map.put(36, "KEYCODE_HOME");
map.put(37, "KEYCODE_DPAD_LEFT");
map.put(38, "KEYCODE_DPAD_UP");
map.put(39, "KEYCODE_DPAD_RIGHT");
map.put(40, "KEYCODE_DPAD_DOWN");
map.put(45, "KEYCODE_INSERT");
map.put(46, "KEYCODE_FORWARD_DEL");
map.put(48, "KEYCODE_0");
map.put(49, "KEYCODE_1");
map.put(50, "KEYCODE_2");
map.put(51, "KEYCODE_3");
map.put(52, "KEYCODE_4");
map.put(53, "KEYCODE_5");
map.put(54, "KEYCODE_6");
map.put(55, "KEYCODE_7");
map.put(56, "KEYCODE_8");
map.put(57, "KEYCODE_9");
map.put(65, "KEYCODE_A");
map.put(66, "KEYCODE_B");
map.put(67, "KEYCODE_C");
map.put(68, "KEYCODE_D");
map.put(69, "KEYCODE_E");
map.put(70, "KEYCODE_F");
map.put(71, "KEYCODE_G");
map.put(72, "KEYCODE_H");
map.put(73, "KEYCODE_I");
map.put(74, "KEYCODE_J");
map.put(75, "KEYCODE_K");
map.put(76, "KEYCODE_L");
map.put(77, "KEYCODE_M");
map.put(78, "KEYCODE_N");
map.put(79, "KEYCODE_O");
map.put(80, "KEYCODE_P");
map.put(81, "KEYCODE_Q");
map.put(82, "KEYCODE_R");
map.put(83, "KEYCODE_S");
map.put(84, "KEYCODE_T");
map.put(85, "KEYCODE_U");
map.put(86, "KEYCODE_V");
map.put(87, "KEYCODE_W");
map.put(88, "KEYCODE_X");
map.put(89, "KEYCODE_Y");
map.put(90, "KEYCODE_Z");
map.put(91, "KEYCODE_WINDOW");
map.put(96, "KEYCODE_0");
map.put(97, "KEYCODE_1");
map.put(98, "KEYCODE_2");
map.put(99, "KEYCODE_3");
map.put(100, "KEYCODE_4");
map.put(101, "KEYCODE_5");
map.put(102, "KEYCODE_6");
map.put(103, "KEYCODE_7");
map.put(104, "KEYCODE_8");
map.put(105, "KEYCODE_9");
map.put(106, "KEYCODE_NUMPAD_MULTIPLY");
map.put(107, "KEYCODE_NUMPAD_ADD");
map.put(109, "KEYCODE_NUMPAD_SUBTRACT");
map.put(110, "KEYCODE_NUMPAD_DOT");
map.put(111, "KEYCODE_NUMPAD_DIVIDE");
map.put(112, "KEYCODE_F1");
map.put(113, "KEYCODE_F2");
map.put(114, "KEYCODE_F3");
map.put(115, "KEYCODE_F4");
map.put(116, "KEYCODE_F5");
map.put(117, "KEYCODE_F6");
map.put(118, "KEYCODE_F7");
map.put(119, "KEYCODE_F8");
map.put(120, "KEYCODE_F9");
map.put(121, "KEYCODE_F10");
map.put(122, "KEYCODE_F11");
map.put(123, "KEYCODE_F12");
map.put(144, "KEYCODE_NUM_LOCK");
map.put(145, "KEYCODE_SCROLL_LOCK");
map.put(186, "KEYCODE_SEMICOLON");
map.put(187, "KEYCODE_EQUALS");
map.put(188, "KEYCODE_COMMA");
map.put(189, "KEYCODE_MINUS");
map.put(190, "KEYCODE_PERIOD");
map.put(191, "KEYCODE_SLASH");
map.put(192, "KEYCODE_GRAVE");
map.put(219, "KEYCODE_LEFT_BRACKET");
map.put(220, "KEYCODE_BACKSLASH");
map.put(221, "KEYCODE_RIGHT_BRACKET");
map.put(222, "KEYCODE_APOSTROPHE");
