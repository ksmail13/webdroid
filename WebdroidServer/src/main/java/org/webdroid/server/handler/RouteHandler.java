package org.webdroid.server.handler;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.ResultMessage;
import org.webdroid.constant.ServerConfigure;
import org.webdroid.db.DBConnector;
import org.webdroid.util.ConsoleLogger;
import org.webdroid.util.JsonUtil;

/**
 * route handler wrapper
 * Created by Micky Kim on 2015-08-17.
 */
public abstract class RouteHandler implements Handler<RoutingContext> {
    protected ConsoleLogger logger = ConsoleLogger.createLogger(getClass());
    protected HttpServerRequest req;
    protected HttpServerResponse res;
    protected Session session;
    protected RoutingContext context = null;
    protected Vertx vertx = null;
    protected DBConnector mDBConnector = null;
    protected boolean needLogin = false;

    /**
     * create route handler
     * @param dbConnector database connect object
     * @param needLogin is this request need authorize
     */
    public RouteHandler(DBConnector dbConnector, boolean needLogin) {
        mDBConnector = dbConnector;
        this.needLogin = needLogin;
    }

    /**
     * handling user request
     * @param routingContext context
     */
    @Override
    public void handle(RoutingContext routingContext) {
        this.context = routingContext;
        res = routingContext.response();
        session = routingContext.session();
        req = routingContext.request();
        vertx = context.vertx();
        // set character set utf-8
        res.putHeader(HttpHeaders.ACCEPT_CHARSET, ServerConfigure.SERVER_ENCODING);
        try {
            if(needLogin && !isLogin()) {
                unAuthorizedHandling();
            } else {
                handling();
            }
        } catch (Exception e) {
            logger.error("throw un catched Exception", e);
            sendJsonResult(HttpStatusCode.RUNTIME_ERROR, false, ResultMessage.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * send response
     * @param statusCode status code
     * @param contentType data type (eg. html, json)
     * @param data data string
     */
    public final void send(int statusCode, String contentType, String data) {
        res.setStatusCode(statusCode);
        res.putHeader(HttpHeaders.CONTENT_TYPE, contentType);
        res.end(data);
    }

    /**
     * send response
     * @param statusCode status code
     * @param contextType data type
     * @param data data buffer
     */
    public final void send(int statusCode, String contextType, Buffer data) {
        res.setStatusCode(statusCode);
        res.putHeader(HttpHeaders.CONTENT_TYPE, contextType);
        res.end(data);
    }

    /**
     * res json createJsonResult
     * @param statusCode req createJsonResult code
     * @param result req createJsonResult
     * @param message req message
     */
    public final void sendJsonResult(int statusCode, boolean result, String message) {
        send(statusCode, "application/json", JsonUtil.createJsonResult(result, message).toString());
    }

    /**
     * redirect page
     * @param url url
     */
    public final void redirectTo(String url) {
        logger.debug("redirect to " +url);
        res.setStatusCode(HttpStatusCode.FOUND);
        res.putHeader("location", url);
        res.end();
    }

    /**
     * check user login
     * @return login status
     */
    public final boolean isLogin() {
        return Boolean.TRUE.equals(session.get("is_enable"));
    }

    /**
     * routing handling
     */
    public abstract void handling();

    /**
     * call when unAuthrized Request from client
     */
    public abstract void unAuthorizedHandling();
}