package org.webdroid.server.handler;

import io.vertx.ext.web.templ.JadeTemplateEngine;
import io.vertx.ext.web.templ.TemplateEngine;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.ServerConfigure;
import org.webdroid.db.DBConnector;
import org.webdroid.server.url.page.SigninPageHandler;

/**
 * page request handler base
 * Created by Micky Kim on 2015-08-17.
 */
public abstract class PageHandler extends RouteHandler {

    /**
     * basic template engine
     */
    protected JadeTemplateEngine templateEngine = null;

    /**
     * create page handler
     * @param dbConnector database connect object
     * @param needLogin login check flag
     */
    public PageHandler(DBConnector dbConnector, boolean needLogin) {
        super(dbConnector, needLogin);
        templateEngine = JadeTemplateEngine.create();
        if(ServerConfigure.DEBUG)
            templateEngine.getJadeConfiguration().setCaching(false);
    }

    /**
     * rendering template with data
     * @param path template file path
     */
    public void rendering(String path) {
        context.put("islogin", isLogin());
        templateEngine.render(context, path, renderRes -> {
            if(renderRes.succeeded()) {
                send(HttpStatusCode.SUCCESS, "text/html", renderRes.result().toString());
            } else {
                logger.error("render error ", renderRes.cause());
                send(HttpStatusCode.RUNTIME_ERROR, "text/html", renderRes.cause().getMessage());
            }
        });

    }

    /**
     * unauthorized access
     */
    @Override
    public void unAuthorizedHandling() {
        redirectTo(SigninPageHandler.URL);
    }
}
