package org.webdroid.server.handler;

import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.ResultMessage;
import org.webdroid.db.DBConnector;

import java.util.HashMap;
import java.util.Map;

/**
 * data request handler
 * Created by Micky Kim on 2015-08-17.
 */
public abstract class RequestHandler extends RouteHandler {
    protected String[] requestParameters = null;


    /**
     * Request handler not web page
     * @param checkLogin this request need auth
     * @param params parameter names.
     */
    public RequestHandler (DBConnector dbConnector, boolean checkLogin, String... params) {
        super(dbConnector, checkLogin);
        requestParameters = params;
    }

    /**
     * Reqeust handler not web page
     * @param checkLogin this request need auth
     */
    public RequestHandler (DBConnector dbConnector, boolean checkLogin) {
        this(dbConnector, checkLogin, null);
    }

    /**
     * call when server got request
     */
    @Override
    public void handling() {
        if(requestParameters == null) {
            handlingWithParams(null);
            return ;
        }

        HashMap<String, Object> params = new HashMap<>();
        // Check each parameters
        for (String requestParameter : requestParameters) {
            String receiveParam = req.getParam(requestParameter);
            if(receiveParam != null)
                params.put(requestParameter, receiveParam);
            else {
                reqRecvParamsLess(requestParameter);
                return ;
            }
        }
        handlingWithParams(params);
    }

    @Override
    public void unAuthorizedHandling() {
        sendJsonResult(HttpStatusCode.UNAUTHORIZED_ACCESS, false, ResultMessage.UNAUTHORIZED_ACCESS);
    }

    /**
     * This method will execute when parameter is not enough
     */
    public void reqRecvParamsLess(String paramKey) {
        sendJsonResult(200, false, "you should include parameter "+paramKey);
    }

    /**
     * request handling
     * @param params parameters from client
     */
    public abstract void handlingWithParams(Map<String, Object> params);
}
