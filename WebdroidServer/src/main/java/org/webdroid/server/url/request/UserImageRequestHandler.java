package org.webdroid.server.url.request;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JsonUtil;

import java.util.Map;

/**
 * Created by micky on 2015. 9. 7..
 */
public class UserImageRequestHandler extends RequestHandler {

    public static final String URL = "/profile/image/:id";

    Vertx vertx = null;

    public UserImageRequestHandler(DBConnector dbConnector, Vertx vertx) {
        super(dbConnector, false, "id");

        this.vertx = vertx;
    }

    @Override
    public void handlingWithParams(Map<String, Object> params) {
        JsonArray reqParams = JsonUtil.createJsonArray(params.get("id"));

        mDBConnector.query(Query.GET_PROFILE_IMAGE, reqParams, new SQLResultHandler<ResultSet>(this) {
            @Override
            public void success(ResultSet resultSet) {
                String path = resultSet.getRows().get(0).getString("path");
                if(path != null) {
                    logger.debug("user image path "+path);
                    Buffer image = vertx.fileSystem().readFileBlocking(path);
                    String ext = path.substring(path.lastIndexOf(".")+1);
                    logger.debug("user profile image " + path +" ext : " + ext);
                    send(HttpStatusCode.SUCCESS, "image/"+ext, image);
                }
            }
        });
    }
}
