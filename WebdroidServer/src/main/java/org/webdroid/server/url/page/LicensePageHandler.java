package org.webdroid.server.url.page;

import org.webdroid.constant.WebdroidConstant;
import org.webdroid.server.handler.PageHandler;

/**
 * Created by Y on 2015-10-01.
 * page handler for license page.
 */
public class LicensePageHandler extends PageHandler {

    public static final String URL = "/license";

    public LicensePageHandler() {
        super(null, false);
    }

    @Override
    public void handling() {

        rendering(WebdroidConstant.Path.HTML + "/license");
    }
}

