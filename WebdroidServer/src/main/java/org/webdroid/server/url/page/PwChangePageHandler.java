package org.webdroid.server.url.page;

import org.webdroid.constant.WebdroidConstant;
import org.webdroid.server.handler.PageHandler;

/**
 * Created by yoonju on 2015. 9. 20.
 */
public class PwChangePageHandler extends PageHandler {

    public static final String URL = "/pwfind";


    public PwChangePageHandler() {
        super(null, false);
    }

    @Override
    public void handling() {
        rendering(WebdroidConstant.Path.HTML + "/pwfind");
    }
}
