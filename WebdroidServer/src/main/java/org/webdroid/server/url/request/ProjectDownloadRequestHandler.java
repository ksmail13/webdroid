package org.webdroid.server.url.request;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.web.FileUpload;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.constant.ResultMessage;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JqueryFileTree;
import org.webdroid.util.JsonUtil;
import java.util.Map;

/**
 * Created by Yoonju on 2015-09-24.
 */
public class ProjectDownloadRequestHandler extends RequestHandler{

    public static final String URL = "/filedown/:pid";

    private Vertx vertx = null;

    FileUpload upload;

    public ProjectDownloadRequestHandler(DBConnector dbConnector, Vertx vertx) {

        super(dbConnector, false, "pid");
        this.vertx = vertx;
    }

    @Override
    public void unAuthorizedHandling() {
        sendJsonResult(HttpStatusCode.UNAUTHORIZED_ACCESS, false, ResultMessage.UNAUTHORIZED_ACCESS);
    }

    @Override
    public void handlingWithParams(Map<String, Object> params) {

        JsonArray reqParams = JsonUtil.createJsonArray(params.get("pid"));

        mDBConnector.query(Query.GET_PATH, reqParams, new SQLResultHandler<ResultSet>(this) {
            @Override
            public void success(ResultSet resultSet) {
                String path = resultSet.getRows().get(0).getString("p_path");
                if (path != null) {
                    logger.debug("########user project path " + path);
                    String filepath =path.substring(0, path.lastIndexOf('/'));
                    String zip=".zip";
                    String fullpath=filepath+zip;

                    Buffer filebuf = vertx.fileSystem().readFileBlocking(fullpath);

                    logger.debug("########path " + path + " filepath : " + filepath+"fullpath"+ fullpath);
                    send(HttpStatusCode.SUCCESS, "application/zip", filebuf);
                }
            }


        });

    }

}





