package org.webdroid.server.url.page;

import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;
import org.webdroid.constant.Query;
import org.webdroid.constant.WebdroidConstant;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.PageHandler;
import org.webdroid.util.JsonUtil;

/**
 * Created by micky on 2015. 9. 5..
 *
 */
public class ProjectViewerPageHandler extends PageHandler {

    public static final String URL = "/projectview/:projectid";

    public ProjectViewerPageHandler(DBConnector dbConnector) {
        super(dbConnector, false);
    }

    @Override
    public void handling() {

        String pid = req.getParam("projectid");
        String name = session.get("name");
        if (isLogin())
            context.put("name", name);
        else {
            context.put("name", " SignIn");
        }

        context.put("pid",pid);
        context.put("islogin", isLogin());
        context.put("isprojectview", true);

        if(!isLogin()) {
            JsonArray params = JsonUtil.createJsonArray(pid);
            mDBConnector.query(Query.GET_PROJECT_NAME, params, new SQLResultHandler<ResultSet>(this) {
                @Override
                public void success(ResultSet resultSet) {
                    String pname = resultSet.getRows().get(0).getString("p_name");

                    context.put("isimportant", false);
                    context.put("projectname", pname);
                    rendering(WebdroidConstant.Path.HTML + "/projectview");
                }

            });
        } else {
            JsonArray params = JsonUtil.createJsonArray(pid, session.get("id"), pid);
            mDBConnector.query(Query.GET_PROJECT_INFO, params, new SQLResultHandler<ResultSet>(this) {
                @Override
                public void success(ResultSet resultSet) {
                    String pname = resultSet.getRows().get(0).getString(resultSet.getColumnNames().get(0));
                    String important = resultSet.getRows().get(1).getString(resultSet.getColumnNames().get(0));

                    context.put("isimportant", important.equals("1"));
                    context.put("projectname", pname);
                    rendering(WebdroidConstant.Path.HTML + "/projectview");
                }
            });
        }
    }
}
