package org.webdroid.server.url.request;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.datagram.DatagramPacket;
import io.vertx.core.datagram.DatagramSocket;
import io.vertx.core.datagram.PacketWritestream;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSSocket;
import org.webdroid.util.ConsoleLogger;

/**
 * Created by micky on 2015-10-01.
 */
public class VMFramebufferRequestHandler {

    public static final String URL = "/getFrameBuffer/*";


    Vertx vertx;
    SockJSHandler handler;
    private ConsoleLogger logger = ConsoleLogger.createLogger(getClass());

    public VMFramebufferRequestHandler(Vertx vertx) {
        this.vertx = vertx;
        handler = SockJSHandler.create(vertx);
        handler.socketHandler(this::connectHandler);
    }

    private void connectHandler(SockJSSocket sockJSSocket) {
        final String vmIp = "127.0.0.1";
        final int vmPort = 1110;
        int port = vmPort + (Integer)sockJSSocket.webSession().data().get("id");
        new UserVMPair(sockJSSocket, vertx.createDatagramSocket(), vmIp, port);
        sockJSSocket.write(Buffer.buffer("start"));
        /*vertx.eventBus().send("user_vm_addr", id,response-> {
            if(response.succeeded()) {
                String addr = String.valueOf(response.result().body());
                logger.debug(String.format("user %s vm address %s", id, addr));
                String ip = addr.split(":")[0];
                int port = Integer.parseInt(addr.split(":")[1]);
                new UserVMPair(sockJSSocket, vertx.createDatagramSocket(), ip, port);
                sockJSSocket.write(Buffer.buffer("start"));
            }
        });*/
    }

    public SockJSHandler getHandler() {
        return handler;
    }
}


/**
 * Manage connect state between user(client) and vm
 */
class UserVMPair {
    PacketWritestream sender;
    DatagramSocket vmSocket;
    SockJSSocket clntSocket;

    int pairState = -1;
    int cnt=0;
    private ConsoleLogger logger = ConsoleLogger.createLogger(getClass());

    public UserVMPair(SockJSSocket clntSocket, DatagramSocket vmSocket, String ip, int port) {
        logger.debug("create framebuffer pipe "+port);

        this.clntSocket = clntSocket;
        this.vmSocket = vmSocket;
        this.sender = vmSocket.sender(port, ip);

        vmSocket.exceptionHandler(this::vmSocketErrorHandler);
        vmSocket.handler(this::vmSocketReceiveHandler);
        clntSocket.drainHandler(this::sockjsDrainHandler);
        clntSocket.handler(this::sockJsReceivedHandler);
        clntSocket.endHandler(this::sockJsEndedHandler);

        //sender.write(Buffer.buffer("start"));
    }

    private void sockjsDrainHandler(Void aVoid) {
        System.out.println("drain!");
    }

    private void vmSocketErrorHandler(Throwable throwable) {
        throwable.printStackTrace();
    }


    private void vmSocketReceiveHandler(DatagramPacket packet) {
        //logger.debug("receive packet from vm" + cnt++ + " length " + packet.data().length());
        switch (pairState) {
            case 1:
                clntSocket.write(Buffer.buffer(String.valueOf(packet.data().getInt(0))));
                break;
            case 2:
                clntSocket.write(packet.data());
                break;
        }
    }

    /**
     * when user close sockjs page
     * @param v nothing
     */
    private void sockJsEndedHandler(Void v) {
        logger.debug("user disconnected");
        vmSocket.close();
    }

    /**
     * when user send data
     * user send start vm will send framebuffer size
     * user send ok vm will send framebuffer data
     * @param buf data from user
     */
    private void sockJsReceivedHandler(Buffer buf) {
        //logger.debug("receive packet from client");
        String msg = buf.toString("utf-8");
        if("start".equals(msg)) {
            pairState = 1;
        } else if ("ok".equals(msg)){
            pairState = 2;
        }
        //logger.debug("send "+msg);
        sender.write(buf);
    }

}