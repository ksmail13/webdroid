package org.webdroid.server.url.request;

import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.UpdateResult;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.constant.ResultMessage;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JsonUtil;

import java.util.Map;

/**
 * Created by Y on 2015-10-04.
 */
public class ProjectViewCancelFavoriteRequestHandler extends RequestHandler {

    public static final String URL = "/cancel_favorate_projectlist/:pid";

    public ProjectViewCancelFavoriteRequestHandler(DBConnector dbConnector)  {
        super(dbConnector, true, "pid");
    }//database connect

    @Override

    //change the favorite project to the normal project.
    public void handlingWithParams(Map<String, Object> params) {
        JsonArray dbParams = JsonUtil.createJsonArray(params.get("pid"));
        mDBConnector.update(Query.CANCEL_FAVORATE_PROJECT, dbParams,
                new SQLResultHandler<UpdateResult>(this) {
                    @Override
                    public void success(UpdateResult resultSet) {
                        if(resultSet.getUpdated()==1)
                            sendJsonResult(HttpStatusCode.SUCCESS, true, ResultMessage.SUCCESS);
                        else
                            sendJsonResult(HttpStatusCode.SUCCESS, false, ResultMessage.SUCCESS);
                    }
                });
    }


}
