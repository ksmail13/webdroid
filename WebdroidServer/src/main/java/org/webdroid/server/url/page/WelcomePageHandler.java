package org.webdroid.server.url.page;

import org.webdroid.constant.WebdroidConstant;
import org.webdroid.server.handler.PageHandler;

/**
 * Created by micky on 2015. 9. 5..
 */
public class WelcomePageHandler extends PageHandler{

    public final static String URL = "/";

    public WelcomePageHandler() {
        super(null, false);
    }


    @Override
    public void handling() {
        if (isLogin()) {
            redirectTo(UserIndexPageHandler.URL);
            return;
        }

        rendering(WebdroidConstant.Path.HTML + "/welcome");
    }

}
