package org.webdroid.server.url.request;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSSocket;
import org.webdroid.util.ConsoleLogger;
import org.webdroid.util.SyncPipe;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * Created by Owner on 2015-10-02.
 */
public class VMLogCatRequestHandler {
    private ConsoleLogger logger = ConsoleLogger.createLogger(getClass());

    public static final String URL = "/vmLogcat/*";

    private Vertx vertx;
    private SockJSHandler handler;
    private EventBus eb = null;
    private String targetIP = "";

    public VMLogCatRequestHandler(Vertx vertx) {
        this.vertx = vertx;
        this.handler = SockJSHandler.create(vertx);
        eb = this.vertx.eventBus();
        handler.socketHandler(this::sockJSConnected);
    }

    public SockJSHandler getHandler() {
        return handler;
    }

    private void sockJSConnected(SockJSSocket sockJSSocket) {
        try {
            logger.debug(sockJSSocket.webSession().data().toString());
            //String uid = sockJSSocket.webSession().get("id");
        } catch (Exception e) {
            logger.error("error", e);
        }
        String uid = sockJSSocket.webSession().data().get("id").toString();
        logger.debug(sockJSSocket.remoteAddress().host());

        eb.send("user_vm_addr", uid, o -> {
            targetIP = o.result().body().toString();
            logger.debug("IP recv " + targetIP);
        });

        try {
            String[] command = {"cmd",};
            Process p = Runtime.getRuntime().exec(command);
            new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
            new Thread(new LogcatPipe(p.getInputStream(), System.out, sockJSSocket)).start();
            PrintWriter stdin = new PrintWriter(p.getOutputStream());
            stdin.println("adb -s " + targetIP + " logcat");
            stdin.close();
            //int returnCode = p.waitFor();

        } catch (IOException e) {
            e.printStackTrace();
        }

        //sockJSSocket.endHandler(aVoid -> vertx.eventBus().send("vm_event_to_mon", "end_vm#" + uid));
    }
}

class LogcatPipe implements Runnable {
    private final InputStream istrm_;
    private final OutputStream ostrm_;
    private final SockJSSocket sockJSSocket_;

    public LogcatPipe(InputStream istrm, OutputStream ostrm, SockJSSocket sockJSSocket) {
        istrm_ = istrm;
        ostrm_ = ostrm;
        sockJSSocket_ = sockJSSocket;
    }

    public void run() {
        try
        {
            final byte[] buffer = new byte[1024];
            for (int length = 0; (length = istrm_.read(buffer)) != -1; )
            {
                ostrm_.write(buffer, 0, length);
                sockJSSocket_.write(Buffer.buffer().setBytes(0,buffer));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
