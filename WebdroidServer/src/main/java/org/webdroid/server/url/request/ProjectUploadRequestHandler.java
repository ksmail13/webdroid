package org.webdroid.server.url.request;

import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.UpdateResult;
import io.vertx.ext.web.FileUpload;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.constant.ResultMessage;
import org.webdroid.constant.WebdroidConstant;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JsonUtil;
import org.webdroid.util.SimpleRunner;
import org.webdroid.util.UnZipper;

import java.io.*;
import java.util.Map;
import java.util.Set;

/**
 * Handler Uploads file, uzip and build project
 * Created by Seho on 2015. 9. 10..
 */
public class ProjectUploadRequestHandler extends RequestHandler {

    public static final String URL = "/fileup";
    private Vertx vertx = null;
    Set<FileUpload> uploads;
    private EventBus eb = null;
    private int pid;

    public ProjectUploadRequestHandler(DBConnector dbConnector, Vertx vertx) {
        super(dbConnector, true, "p_name", "p_descript");
        mDBConnector = dbConnector;
        this.vertx = vertx;
        eb = this.vertx.eventBus();
        eb.localConsumer("complete_insert", this::startMakeProjectPath);
    }

    private void startMakeProjectPath(Message<Object> objectMessage) {
        logger.debug("build project "+objectMessage.body().toString());
        makeProjectPath(Integer.parseInt(objectMessage.body().toString()));
    }

    @Override
    public void handlingWithParams(Map<String, Object> params) {
        uploads = context.fileUploads();
        if (uploads.size() > 0) {
            JsonArray dbParams = JsonUtil.createJsonArray(params.get("p_name"), params.get("p_descript"), session.get("id"));
            mDBConnector.update(Query.NEW_PROJECT, dbParams, new SQLResultHandler<UpdateResult>(this) {
                @Override
                public void success(UpdateResult result) {
                    logger.debug(result.toJson().toString());
                    JsonObject res = JsonUtil.createJsonResult(true, ResultMessage.SUCCESS);
                    //send(HttpStatusCode.SUCCESS, "application/json", res.toString());
                    pid = result.toJson().getJsonArray("keys").getInteger(0);
                    eb.send("complete_insert", pid);
                }
            });
        } else {
            sendJsonResult(HttpStatusCode.SUCCESS, false, "no file upload");
        }
    }

    public void makeProjectPath(int pid) {
        FileUpload upload = (FileUpload)uploads.toArray()[0];

        System.out.println(upload.fileName());
        String zipFileName = String.format("/%d_%s.zip", pid, req.getParam("p_name"));
        String unzipPathFolderName = String.format("%d_%s", pid, req.getParam("p_name"));


        String zipFilePath = WebdroidConstant.Path.UPLOAD_PROJECT + "/" + zipFileName;
        String unzipPath = WebdroidConstant.Path.UPLOAD_PROJECT + "/" + unzipPathFolderName + "/";

        logger.debug(String.format("project file save in %s", zipFilePath));
        logger.debug(upload);

        Builder b = new Builder(zipFileName, unzipPathFolderName, unzipPath);
        vertx.fileSystem().copy(upload.uploadedFileName(), zipFilePath, b::uploadFileCopyHandler);

    }

    private class Builder {

        String zipFileName, unzipPathFolderName, unzipPath;
        DeliveryOptions options = new DeliveryOptions();
        public Builder(String zipFile, String unzipPathFolderName, String unzipPath) {
            this.zipFileName = zipFile;
            this.unzipPathFolderName = unzipPathFolderName;
            this.unzipPath = unzipPath;
            options.addHeader("session", session.id());

            sendProgress("35");
        }

        public void sendProgress(String message) {
            eb.send("uploadProgress", message, options, o -> logger.debug(o.result().body().toString()));
        }

        public void uploadFileCopyHandler(AsyncResult<Void> copyResult) {
            if(copyResult.failed()) {
                logger.error("copy failed", copyResult.cause());
                sendJsonResult(HttpStatusCode.SUCCESS, false, ResultMessage.INTERNAL_SERVER_ERROR);
                sendProgress("-1");
            } else {
                sendProgress("45");
                vertx.executeBlocking(objectFuture -> {
                    UnZipper.unZipper(zipFileName, unzipPathFolderName);
                    objectFuture.complete();
                }, this::unzipProjectHandler);
            }
        }

        private void unzipProjectHandler(AsyncResult<Object> objectAsyncResult) {
            sendProgress("75");

            vertx.executeBlocking(objectFuture -> {
                boolean buildResult = false;
                try {
                    buildResult = buildProject(unzipPathFolderName);
                } catch (Exception e) {
                    logger.error("error on build and get apk", e);
                    e.printStackTrace();
                } finally {
                    objectFuture.complete(buildResult);
                }
            }, false, this::buildProjectHandler);

            //logger.debug("uploads");

        }

        private void buildProjectHandler(AsyncResult<Object> objectAsyncResult) {
            boolean buildResult = (Boolean)objectAsyncResult.result();
            logger.debug("build result "+objectAsyncResult.toString());
            logger.debug(objectAsyncResult.result());
            if(buildResult) {
                uploads.forEach(upload -> logger.debug(upload.fileName() + " " + upload.uploadedFileName() + " " + upload.name()));

                JsonArray dbParams = JsonUtil.createJsonArray(session.get("id"), pid);
                mDBConnector.update(Query.UPDATE_USER_PROJECT, dbParams, new SQLResultHandler<UpdateResult>() {
                    @Override
                    public void success(UpdateResult result) {
                        logger.debug(result.toJson().toString());
                    }
                });
                dbParams = JsonUtil.createJsonArray(unzipPath, pid);
                mDBConnector.update(Query.SET_PATH, dbParams, new SQLResultHandler<UpdateResult>(ProjectUploadRequestHandler.this) {
                    @Override
                    public void success(UpdateResult result) {
                        logger.debug(result.toJson().toString());
                        sendJsonResult(HttpStatusCode.SUCCESS, true, ResultMessage.SUCCESS);
                    }
                });
            }else{
                sendJsonResult(HttpStatusCode.SUCCESS, false, ResultMessage.ERROR);
            }
        }

        public boolean buildProject(String pPath) {
            boolean buildSuccess = false;
            String buildMessage;
            try {
                File localproperty = new File(WebdroidConstant.Path.UPLOAD_PROJECT + "/" + pPath + "/local.properties");

                String s = "sdk.dir="+WebdroidConstant.Path.ANDROID_HOME_STR_FORM;

                FileOutputStream output = new FileOutputStream(localproperty);
                output.write(s.getBytes());
                output.flush();
                output.close();

                localproperty = new File(WebdroidConstant.Path.UPLOAD_PROJECT + "/" + pPath);
                String lpath = localproperty.getAbsolutePath();
                localproperty = new File(lpath);

                CompileLogic cl = new CompileLogic(localproperty);
                cl.compile();
                buildSuccess = cl.isSuccess();
                buildMessage = cl.message();

                if(!buildSuccess) {
                    logger.debug("build failed " + buildMessage);
                    sendProgress(buildMessage);
                    sendProgress("-1");
                } else {
                    int returnCode = cl.status();
                    logger.debug("Return code = " + returnCode);
                    logger.debug("build done");
                    sendProgress("100");
                }
            }/*catch (InterruptedException e) {
                logger.debug("build failed", e);
            }*/ catch (IOException e) {
                logger.debug("build failed ", e);
            }
            return buildSuccess;
        }
    }

    class CompileLogic {
        final String[] buildOption = {"clean",  "build", "assemble"};

        File location;
        boolean success;
        String errorMessage;
        int processStatus;
        public CompileLogic(File location) {
            this.location = location;
        }

        public void compile() {
            for (int i = 0; i < buildOption.length; i++) {
                SimpleRunner runner = new SimpleRunner("cmd", "/C", "gradlew.bat", buildOption[i]);
                runner.setWorkingDirectory(location);
                try {
                    processStatus = runner.execute();
                    success = runner.getError().size() == 0;
                    if(!success) {
                        runner.getOutput().forEach(s-> {
                            if(s.contains("error:")) {
                                errorMessage = "0"+s;
                            } else if(s.startsWith("BUILD FAILED")) {
                                errorMessage = "-1";
                            }
                        });
                        return ;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public int status() {
            return processStatus;
        }

        public boolean isSuccess() {
            return success;
        }

        public String message() {
            return errorMessage;
        }
    }
}
