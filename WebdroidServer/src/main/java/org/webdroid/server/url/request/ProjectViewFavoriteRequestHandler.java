package org.webdroid.server.url.request;

import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.UpdateResult;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.constant.ResultMessage;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JsonUtil;

import java.util.Map;

/**
 * Created by Y on 2015-10-04.
 */
public class ProjectViewFavoriteRequestHandler extends RequestHandler {
    public static final String URL = "/favorate_projectlist/:pid";

    public ProjectViewFavoriteRequestHandler(DBConnector dbConnector)  {
        super(dbConnector, true,"pid");
    }//database connect

    @Override

    //chage the normal project to favorite project.
    public void handlingWithParams(Map<String, Object> params) {
        JsonArray dbParams = JsonUtil.createJsonArray(session.get("id"), params.get("pid"));

        System.out.println("check:"+session.get("id").toString()+params.get("pid").toString());

        mDBConnector.update(Query.VIEW_FAVORITE_PROJECT, dbParams,
                new SQLResultHandler<UpdateResult>(this) {
                    @Override
                    public void success(UpdateResult resultSet) {
                        //sendJsonResult(HttpStatusCode.SUCCESS, true, ResultMessage.SUCCESS);
                        if (resultSet.getUpdated() > 0) {
                            sendJsonResult(200, true, ResultMessage.SUCCESS);
                            //redirectTo("/");
                        } else
                            sendJsonResult(200, false, ResultMessage.ERROR);

                    }
                });
        }

    }
