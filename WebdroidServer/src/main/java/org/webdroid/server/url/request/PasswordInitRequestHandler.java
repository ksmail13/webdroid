package org.webdroid.server.url.request;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mail.MailClient;
import io.vertx.ext.mail.MailConfig;
import io.vertx.ext.mail.MailMessage;
import io.vertx.ext.mail.StartTLSOptions;
import io.vertx.ext.sql.UpdateResult;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.constant.ResultMessage;
import org.webdroid.constant.WebdroidConstant;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JsonUtil;

import java.util.Map;
import java.util.Random;

/**
 * Created by yooonju on 2015. 9. 20..
 */
public class PasswordInitRequestHandler extends RequestHandler {

    public static final String URL = "/api/pwfind";


    private Vertx vertx = null;

    MailClient mailClient;

    public PasswordInitRequestHandler(DBConnector dbConnector, Vertx vertx) {
       super(dbConnector, false, "user_id");
        this.vertx = vertx;
        MailConfig config = new MailConfig();
        config.setHostname("smtp.gmail.com");
        config.setPort(587);
       // config.setSsl(true);
               // config.setTrustAll(true);
                        //config.setStarttls(StartTLSOptions.REQUIRED);
        config.setUsername("webdroid.noreply");
        config.setPassword("web321droid!@#");
        mailClient = MailClient.createNonShared(vertx, config);
        }

        @Override
        public void handlingWithParams(Map<String, Object> params) {
            String user_id = req.getParam("user_id");

            Random rand = new Random();
            int newpw = rand.nextInt((1000000 -100000 ) + 1) + 100000;  //reset pw

            JsonArray dbParams = JsonUtil.createJsonArray(newpw, params.get("user_id"));
            MailMessage message = new MailMessage();
            message.setFrom("webdroid.noreply@gmail.com");
            message.setTo(user_id);
        //message.setText("this is the plain message text");

            vertx.fileSystem().readFile(WebdroidConstant.Path.STATIC + "/pwreset.html", bufferAsyncResult -> {
                    if (bufferAsyncResult.succeeded()) {
                            message.setHtml(bufferAsyncResult.result().toString().replace("####", String.valueOf(newpw)));

                                    mailClient.sendMail(message, result -> {
                                            if (result.succeeded()) {
                                                    System.out.println(result.result());
                                                    logger.debug("okay");
                                                } else {
                                                    result.cause().printStackTrace();
                                                }
                                        });
                        }
                });

        //message.setHtml("this is html text <a href=\"\">vertx.io</a>");

        mDBConnector.update(Query.SET_RANDOM_PW, dbParams, new SQLResultHandler<UpdateResult>(this) {

            @Override

             public void success(UpdateResult resultSet) {
                if (resultSet.getUpdated() > 0) {
                    sendJsonResult(HttpStatusCode.SUCCESS, true, ResultMessage.SET_RANDOM_PW);
                } else
                    sendJsonResult(HttpStatusCode.SUCCESS, false, ResultMessage.SET_RANDOM_PW_FAIL);
            }
        });
    }
}