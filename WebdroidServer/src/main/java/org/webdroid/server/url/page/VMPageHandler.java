package org.webdroid.server.url.page;

import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;
import org.webdroid.constant.Query;
import org.webdroid.constant.WebdroidConstant;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.PageHandler;
import org.webdroid.util.JsonUtil;

/**
 * Created by ksmai on 2015-09-30.
 */
public class VMPageHandler extends PageHandler {
    public static final String URL = "/remotedevice/:projectid";

    public VMPageHandler(DBConnector dbConnector) {
        super(dbConnector, true);
    }

    @Override
    public void handling() {
        String pid = req.getParam("projectid");
        JsonArray params = JsonUtil.createJsonArray(pid);
        mDBConnector.query(Query.GET_PROJECT_NAME, params, new SQLResultHandler<ResultSet>(this) {
            @Override
            public void success(ResultSet resultSet) {
                String pname = resultSet.getRows().get(0).getString("p_name");
                context.put("pname", pname);
                context.put("pid", pid);
                rendering(WebdroidConstant.Path.HTML + "/device_displayer");
            }
        });
    }
}
