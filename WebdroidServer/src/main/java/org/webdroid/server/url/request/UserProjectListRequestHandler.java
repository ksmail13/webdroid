package org.webdroid.server.url.request;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JsonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by minsoo on 2015-09-09.
 * This class makes projectlists. Projectlists are divided 'projects' and 'favorates'.
 * 'projects' are normal projects showing in the MY PROJECT.
 * 'favorates' are user's favorite projects showing in th MY FAVORITE.
 */
public class UserProjectListRequestHandler extends RequestHandler {

    public static final String URL = "/show_projectlist";

    public UserProjectListRequestHandler(DBConnector dbConnector) {
        super(dbConnector, true);
    }//database connect
    @Override

    //To show projectlist, use user's id. Favorite project and normal project are separated
    public void handlingWithParams(Map<String, Object> params) {


        JsonArray dbParams = JsonUtil.createJsonArray(session.get("id").toString());
        mDBConnector.query(Query.MY_PROJECT, dbParams,
                new SQLResultHandler<ResultSet>(this) {
                    @Override
                    public void success(ResultSet resultSet) {
                        logger.debug("request "+dbParams);
                        List<JsonObject> resultList = resultSet.getRows();
                        List<JsonObject> filteredList = new ArrayList<>(resultList);
                        logger.debug(resultList);
                        JsonObject row = new JsonObject();

                        resultList.removeIf(obj -> obj.getInteger("is_important", 1) == 1);
                        row.put("projects", new JsonArray(resultList));

                        filteredList.removeIf(obj -> obj.getInteger("is_important", 0) == 0);
                        row.put("favorates", new JsonArray(filteredList));

                        send(HttpStatusCode.SUCCESS, "application/json", row.toString());
                    }
                });
    }

}
