package org.webdroid.server.url.request;

import io.vertx.core.file.FileSystem;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.ResultSet;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.constant.WebdroidConstant;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JsonUtil;

import java.util.Map;

/**
 * Created by ��μ� on 2015-10-02.
 * This class get a project image. This image appears on the projectlist.
 */
public class RequestProjectImageHandler extends RequestHandler {
    public static final String URL = "/projimg/:pid";

    public RequestProjectImageHandler(DBConnector dbConnector)  {
        super(dbConnector, false);
    } //database connect

    @Override
    //get a project image.
    public void handlingWithParams(Map<String, Object> params) {

        String pid = req.getParam("pid");

        JsonArray Params = JsonUtil.createJsonArray(pid);
        mDBConnector.query(Query.GET_PATH, Params, new SQLResultHandler<ResultSet>(this) {
            @Override
            public void success(ResultSet resultSet) {
                String ppath = resultSet.getRows().get(0).getString("p_path")+"/app/src/main/res/mipmap-xxhdpi/ic_launcher.png";
                FileSystem fs = context.vertx().fileSystem();
                if(fs.existsBlocking(ppath))
                {
                    res.sendFile(ppath);
                    /*fs.readFile(ppath, bufferAsyncResult -> {
                        if (bufferAsyncResult.succeeded()) {
                            send(HttpStatusCode.SUCCESS, "image/png", bufferAsyncResult.result());
                        }
                        else
                        {
                            res.sendFile(WebdroidConstant.Path.IMG_PATH+"/apple-touch-icon@2.png");
                        }
                    });*/

                }
                else
                {
                    res.sendFile(WebdroidConstant.Path.IMG_PATH+"/apple-touch-icon@2.png");
                }

            }

        });
    }
}
