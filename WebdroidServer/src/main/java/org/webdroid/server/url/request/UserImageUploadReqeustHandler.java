package org.webdroid.server.url.request;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.UpdateResult;
import io.vertx.ext.web.FileUpload;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.constant.ResultMessage;
import org.webdroid.constant.WebdroidConstant;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RouteHandler;
import org.webdroid.util.JsonUtil;

import java.util.Calendar;
import java.util.Set;

/**
 * Created by micky on 2015. 9. 5..
 */
public class UserImageUploadReqeustHandler extends RouteHandler {

    public static final String URL = "/p_img_upload";

    private Vertx vertx = null;

    FileUpload upload;

    public UserImageUploadReqeustHandler(DBConnector dbConnector, Vertx vertx) {
        super(dbConnector, true);
        this.vertx = vertx;
    }

    @Override
    public void handling() {
        if(!isLogin()) {
            sendJsonResult(HttpStatusCode.UNAUTHORIZED_ACCESS, false, ResultMessage.UNAUTHORIZED_ACCESS);
            return;
        }

        Set<FileUpload> uploads = context.fileUploads();
        uploads.stream().forEach(upload -> this.upload = upload);
        Calendar c = Calendar.getInstance();
        String contentType = upload.contentType();
        String upfileExt = contentType.substring(contentType.indexOf('/')+1);

        String pImagePath = WebdroidConstant.Path.UPLOAD_IMG+"/"+String.format("%d_profile_%d.%s",
                (Integer)session.get("id"), c.getTimeInMillis(), upfileExt);
        logger.debug(String.format("file save in %s", pImagePath));
        logger.debug(upload);

        vertx.fileSystem().copyBlocking(upload.uploadedFileName(), pImagePath);

        JsonArray dbParams = JsonUtil.createJsonArray(pImagePath, session.get("id"));
        mDBConnector.update(Query.PROFILE_IMG_UPLOAD, dbParams, new SQLResultHandler<UpdateResult>(this) {
            @Override
            public void success(UpdateResult resultSet) {
                JsonObject res = JsonUtil.createJsonResult(true, ResultMessage.SUCCESS).put("uploadPath", "/profile/image/"+session.get("id"));
                send(HttpStatusCode.SUCCESS, "application/json", res.toString());
            }
        });
    }

    @Override
    public void unAuthorizedHandling() {
        sendJsonResult(HttpStatusCode.UNAUTHORIZED_ACCESS, false, ResultMessage.UNAUTHORIZED_ACCESS);
    }
}
