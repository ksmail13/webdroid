package org.webdroid.server.url.request;

import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.net.SocketAddress;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSSocket;
import org.webdroid.util.ConsoleLogger;
import org.webdroid.util.Log;

import java.util.HashMap;

/**
 * Created by Seho on 2015-10-01.
 */
public class ProjectUploadProgressBarRequestHandler {
    ConsoleLogger logger = ConsoleLogger.createLogger(getClass());

    public static final String URL = "/fileup/*";

    private Vertx vertx;
    private SockJSHandler handler;
    private EventBus eb = null;
    Buffer b = Buffer.buffer();

    HashMap<String, SockJSSocket> sessionSockMap = new HashMap<>();
    public ProjectUploadProgressBarRequestHandler(Vertx vertx) {
        this.vertx = vertx;
        this.handler = SockJSHandler.create(vertx);
        eb = this.vertx.eventBus();
        handler.socketHandler(this::sockJSConnected);

        eb.consumer("uploadProgress", msg -> {
            String from = msg.headers().get("session");
            SockJSSocket sockJSSocket = sessionSockMap.get(from);
            String sndbuf = msg.body().toString();
            b = Buffer.buffer().setString(0, sndbuf);
            sockJSSocket.write(b);
            msg.reply("#######send " + msg.body().toString());
        });
    }

    public SockJSHandler getHandler() {
        return handler;
    }

    private void sockJSConnected(SockJSSocket sockJSSocket) {
        Log.logging("#######sock connect");

        sessionSockMap.put(sockJSSocket.webSession().id(), sockJSSocket);
        logger.debug("session id connect " + sockJSSocket.webSession().id());
        sockJSSocket.endHandler(sockJsEndHandler(sockJSSocket));
        sockJSSocket.drainHandler(this::sockJsDrainHandler);
    }

    private Handler<Void> sockJsEndHandler(SockJSSocket sockJSSocket) {
        return aVoid -> sessionSockMap.remove(sockJSSocket.webSession().id());
    }

    private void sockJsDrainHandler(Void aVoid) {

    }


}
