package org.webdroid.server.url.request;

import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.UpdateResult;
import org.webdroid.constant.HttpStatusCode;
import org.webdroid.constant.Query;
import org.webdroid.constant.ResultMessage;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JsonUtil;

import java.util.Map;

/**
 * Created by Yoonju on 2015-09-21.
 */
public class IsEnableRequestHandler extends RequestHandler {
    public static final String URL = "/enable";


    public IsEnableRequestHandler(DBConnector dbConnector) { super(dbConnector, false, "user_id");
    }

    @Override
    public void handlingWithParams(Map<String, Object> params) {
        JsonArray dbParams = JsonUtil.createJsonArray(params.get("user_id"));

        mDBConnector.update(Query.UPDATE_IS_ENABLE, dbParams, new SQLResultHandler<UpdateResult>(this) {
            @Override
            public void success(UpdateResult result) {
                if (result.getUpdated() > 0) {
                    session.put("is_enable", true);
                    sendJsonResult(HttpStatusCode.SUCCESS, true, ResultMessage.SUCCESS);

                } else
                    sendJsonResult(HttpStatusCode.SUCCESS, false, ResultMessage.ERROR);
            }
        });
    }
}
