package org.webdroid.server.url.request;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import io.vertx.ext.web.handler.sockjs.SockJSSocket;
import org.webdroid.util.ConsoleLogger;

import java.util.HashMap;

/**
 * Handler to control VM events sent from clients
 * Created by micky kim on 2015-09-30.
 */
public class VMEventControlReqeustHandler{
    private ConsoleLogger logger = ConsoleLogger.createLogger(getClass());

    public static final String URL = "/vmeventcontrol/*";

    private Vertx vertx;
    private SockJSHandler handler;

    // key : uid string, value : user connected sockjssocket
    HashMap<String, SockJSSocket> userSocketMap = null;

    public VMEventControlReqeustHandler(Vertx vertx) {
        this.vertx = vertx;
        this.handler = SockJSHandler.create(vertx);
        handler.socketHandler(this::sockJSConnected);

        vertx.eventBus().consumer("run_vm_success", this::onVmStart);

        userSocketMap = new HashMap<>();
    }

    private void onVmStart(Message<Object> objectMessage) {
        String[] msg = objectMessage.body().toString().split("#");
        logger.debug("uid : " + msg[1]+" "+msg[0]);
        userSocketMap.get(msg[1]).write(Buffer.buffer("run_vm_success"));
    }

    public SockJSHandler getHandler() {
        return handler;
    }

    private void sockJSConnected(SockJSSocket sockJSSocket) {
        // connection initializing
        try {
            logger.debug(sockJSSocket.webSession().data().toString());
            //String uid = sockJSSocket.webSession().get("id");
        } catch (Exception e) {
            logger.error("error", e);
        }
        String uid = sockJSSocket.webSession().data().get("id").toString();
        userSocketMap.put(uid, sockJSSocket);

        logger.debug(sockJSSocket.remoteAddress().host());
        DeliveryOptions options = new DeliveryOptions();
        options.setSendTimeout(200000);
        vertx.eventBus().send("vm_event_to_mon", String.format("run_vm#%s", uid), runVmReply(sockJSSocket));

        sockJSSocket.handler(buffer -> { // user->monkey event deliver
            logger.debug("recv " + buffer.toString());
            vertx.eventBus().send("vm_event_to_mon", "event_vm#" + buffer + "#" + uid);
        });

        sockJSSocket.endHandler(aVoid -> {
            vertx.eventBus().send("vm_event_to_mon", "end_vm#" + uid);
            userSocketMap.remove(uid);
        });
    }

    /**
     * after execute virtual machine reply to client
     * @param jsSocket client socket
     * @return action
     */
    private Handler<AsyncResult<Message<Object>>> runVmReply(SockJSSocket jsSocket) {
        return reply->{
            if(reply.succeeded()) {
                jsSocket.write(Buffer.buffer(reply.result().body().toString()));
            }
        };
    }

}
