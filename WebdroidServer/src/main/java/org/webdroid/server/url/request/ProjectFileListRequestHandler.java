package org.webdroid.server.url.request;

import io.vertx.core.Vertx;
import org.webdroid.db.DBConnector;
import org.webdroid.server.handler.RequestHandler;
import org.webdroid.util.JqueryFileTree;

import java.util.Map;

/**
 * Created by micky on 2015. 9. 5..
 */
public class ProjectFileListRequestHandler extends RequestHandler {

    public static final String URL = "/make_filetree";
    private Vertx vertx = null;

    public ProjectFileListRequestHandler(DBConnector dbConnector, Vertx vertx) {
        super(dbConnector, false, "dir", "root");
        this.vertx = vertx;
    }

    @Override
    public void handlingWithParams(Map<String, Object> params) {
        vertx.executeBlocking(f -> {
            res.setChunked(true);
            res.write(JqueryFileTree.createHtmlRes(req.getParam("dir"), req.getParam("root"))).end();
        }, o -> {});
    }
}
