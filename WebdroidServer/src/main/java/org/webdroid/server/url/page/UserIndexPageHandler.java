package org.webdroid.server.url.page;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import org.webdroid.constant.Query;
import org.webdroid.constant.ResultMessage;
import org.webdroid.constant.WebdroidConstant;
import org.webdroid.db.DBConnector;
import org.webdroid.db.SQLResultHandler;
import org.webdroid.server.handler.PageHandler;
import org.webdroid.util.JsonUtil;

/**
 * Created by micky on 2015. 9. 5..
 */
public class UserIndexPageHandler extends PageHandler {

    public static final String URL = "/projectmain";

    public UserIndexPageHandler(DBConnector dbConnector) {
        super(dbConnector, true);
    }

    @Override
    public void handling() {

        context.put("name", session.get("name"));
        JsonArray params = JsonUtil.createJsonArray((Integer) session.get("id"));

        mDBConnector.query(Query.USER_PROFILE, params, new SQLResultHandler<ResultSet>(this) {
            @Override
            public void success(ResultSet resultSet) {
                JsonObject row = resultSet.getRows().get(0);

                String[] keys = {"id", "git_id", "introduce"};
                String[] errorDefault = {ResultMessage.ID_ERROR, ResultMessage.GIT_ERROR, ResultMessage.INTRODUCE_ERROR, ""};

                for (int i = 0; i < keys.length; i++) {
                    String data = row.getString(keys[i]);
                    logger.debug(data);
                    if (data == null) {
                        context.put("show_"+keys[i], errorDefault[i]);
                    } else {
                        context.put("show_"+keys[i], data);
                    }
                }

                context.put("show_user_img", "profile/image/"+session.get("id"));

                rendering(WebdroidConstant.Path.HTML + "/usermain");  //jade변환한 파일이름
            }
        });
    }
}
