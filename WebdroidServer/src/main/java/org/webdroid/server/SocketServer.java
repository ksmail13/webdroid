package org.webdroid.server;

import io.vertx.core.AsyncResult;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.net.NetClient;
import io.vertx.core.net.NetSocket;
import org.webdroid.constant.WebdroidConstant;
import org.webdroid.util.SimpleRunner;
import org.webdroid.vm.MonkeyController;

import java.io.*;
import java.util.HashMap;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Socket Server ask Python Server to run VM then make clients communicate with MonkeyController instance
 * Created by Seho on 2015-09-07.
 */
public class SocketServer extends WebdroidVerticle {

    private final static int PY_PORT = 1038;
    private final static String PY_IP = "localhost";//"172.16.162.69";//"211.243.108.156";
    public static final String AAPT = "C:/Users/ksmai/Documents/androidsdk/build-tools/23.0.1/aapt";
    NetClient netClient;
    Optional<NetSocket> socket = Optional.empty();
    private boolean frameBufferRecvState = false;
    private String frameBufferRecved = "";
    MonkeyController monkey;
    Buffer b = Buffer.buffer();

    HashMap<String, Consumer<Object>> runReserved;

    public void start(){
        runReserved = new HashMap<>();
        EventBus eb = vertx.eventBus();
        netClient = vertx.createNetClient();
        netClient.connect(PY_PORT, PY_IP, this::connectHandler);
        eb.consumer("socket", this::vmEventHandler);
        eb.consumer("vm_event_to_mon", this::vmEventHandler);
        eb.consumer("user_vm_addr", this::vmAddressHandler);
        monkey = new MonkeyController();
    }

    private void vmAddressHandler(Message<Object> objectMessage) {
        String uid = objectMessage.body().toString();
        String vmIp = monkey.getDeviceIp(Integer.parseInt(uid));
        logger.debug(String.format("user %s request vm ip : %s", uid, vmIp));
        objectMessage.reply(vmIp);
    }

    private void connectHandler(AsyncResult<NetSocket> netSocketAsyncResult) {
        if(netSocketAsyncResult.succeeded()){
            socket = Optional.ofNullable(netSocketAsyncResult.result());
            socket.ifPresent(sock -> {
                logger.info("Connect with python server");
                // data receive
                sock.handler(this::recvHandler);

            });
        } else {
            logger.error("", netSocketAsyncResult.cause());
        }
    }

    private void recvHandler(Buffer buffer) { //run_vm_success#uid#ip //run_vm_fail#uid
        //vertx.eventBus().send("vm_event_to_clnt",buffer);
        String[] message = buffer.toString().split("#");
        String type = message[0];
        logger.debug(buffer.toString());

        // vm connect with adb
        if(type.startsWith("run_vm_success")){
            String[] arr = buffer.toString().split("#");
            int userId = Integer.parseInt(arr[1]);
            vertx.executeBlocking(f -> {
                //monkey.adbConnect("192.168.56.2");//arr[2]);
                //monkey.adbConnect(arr[2]);
                //logger.debug("monkey connect device : " + monkey.connectDevice(Integer.parseInt(arr[1]), "192.168.56.2"));//arr[2])); //����Ǹ� true return //false return exception control
                logger.debug("try connect device");

                // TODO : change way to send vm lunch success message
//                runReserved.get(arr[1]).accept("run_vm_success");
//                runReserved.remove(arr[1]);

                // event message reply
                // can volatilize. Therefore, we use event send
                vertx.eventBus().send(type, buffer);


                try {
                    logger.debug("monkey connect device : " + monkey.connectDevice(Integer.parseInt(arr[1]), arr[2]));
                    monkey.installApk(userId);

                    //get
                    SimpleRunner runner = new SimpleRunner(AAPT, "dump", "badging",
                            monkey.userSelectedPath(userId));
                    runner.execute();

                    Object[] activity =  runner.getOutput().stream().filter(s -> s.startsWith("launchable-activity")).toArray();
                    System.out.println(activity[0]);
                    String parsed = activity[0].toString().replaceAll("launchable-activity: name=\\'", "");
                    parsed = parsed.substring(0, parsed.indexOf('\''));
                    StringBuffer sb= new StringBuffer();
                    sb.append(parsed);
                    sb.insert(parsed.lastIndexOf('.'), '/');
                    SimpleRunner open = new SimpleRunner("adb -s "+monkey.getDeviceIp(userId), "shell", "am", "start", "-n", sb.toString());
                    open.execute();
                } catch (Exception e) {
                    logger.error("catch", e);
                }
                f.complete();
            }, false, o->{
                logger.debug("connect");
            });
            //vertx.eventBus().send("",arr[2]);

        }
        else if(type.contains("Already")){
            //Vm is Aleady Launch

        }
        else if(type.contains("end_vm_success")){
            //Vm end is Success

        }
        else if(type.contains("create_vm")) {
            logger.debug("create vm from vm controller");
        }
        else if(type.contains("open_vm")) {
            logger.debug("create vm from vm controller");
        }
    }

    private void vmEventHandler(Message<Object> objectMessage) {

        logger.debug(objectMessage.body());

        if(objectMessage.body().toString().startsWith("event_vm#touch")){ //event_vm#press?#x#y#flag#uid
            String[] arr;
            arr = objectMessage.body().toString().split("#");
            monkey.getTouchEvent(Integer.parseInt(arr[5]), Integer.parseInt(arr[2]), Integer.parseInt(arr[3]), Integer.parseInt(arr[4]));
        }
        if(objectMessage.body().toString().startsWith("event_vm#key")){ //event_vm#key#keycode#updown?#uid
            String[] arr;
            arr = objectMessage.body().toString().split("#");
            monkey.getPressEvent(Integer.parseInt(arr[4]), arr[2], Integer.parseInt(arr[3]));
        }
        if(objectMessage.body().toString().startsWith("event_vm#apk")){ //event_vm#apk#ppath#uid
            String[] arr = objectMessage.body().toString().split("#");
            monkey.installApkInit(Integer.parseInt(arr[3]), WebdroidConstant.Path.UPLOAD_PROJECT + "/" + arr[2] + "/app/build/outputs/apk/app-debug.apk");
            logger.debug(WebdroidConstant.Path.UPLOAD_PROJECT+"/"+arr[2]+"/app/build/outputs/apk/app-debug.apk");
        }
        if(objectMessage.body().toString().startsWith("end_vm#")){ //end_vm#uid
            String[] arr = objectMessage.body().toString().split("#");
            monkey.disconeectDevice(Integer.parseInt(arr[1]));
            b.setString(0, objectMessage.body().toString());
            socket.ifPresent(sock -> sock.write(b));
        }


        if(objectMessage.body().toString().startsWith("run_vm")){
            socket.ifPresent(sock -> sock.write(objectMessage.body().toString()));
            String[] parsed = objectMessage.body().toString().split("#");
            if(parsed.length >= 2)
                runReserved.put(parsed[1], objectMessage::reply);
            else {
                logger.error("run_vm message has no user id information " + objectMessage.body().toString());
            }
        }

    }
}
