
package org.webdroid.vm;

import com.android.chimpchat.adb.AdbBackend;
import com.android.chimpchat.core.IChimpDevice;
import com.android.chimpchat.core.TouchPressType;
import org.webdroid.util.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * This Class Connect MonkeyRunner and Virtual Device that matching user_id
 * And control events(touch,press,install Apk) from Web Server
 * Using Android's Chimpchat Api
 * Created by Kyle Park on 2015-09-23.
 */
public class MonkeyController {
    private Map<Integer,IChimpDevice> devices;
    private Map<Integer,String> paths;
    //Map's key is userNumber, Value is Device(Connect by MonkeyRunner)
    private Map<Integer,String> ips;
    //Map's key is userNumber, Value is Device's ip address
    private String deviceIp = "localhost:";
    private AdbBackend backend;
    public MonkeyController(){
        devices = new HashMap<Integer,IChimpDevice>();
        ips = new HashMap<Integer,String>();
        paths = new HashMap<Integer,String>();
        backend = new AdbBackend();
    }

    public boolean connectDevice(int userId,String portNum){
        //Coneect Device with monkeyrunner

        IChimpDevice device = backend.waitForConnection(1000, ".*" + portNum);

        if(device == null){
            Log.errorLogging("Monkey Connection Error in " + deviceIp+portNum);
            return false;
        }
        devices.put(userId, device);
        Log.debugLogging("matching "+ userId + " with " + deviceIp+portNum);
        ips.put(userId,deviceIp+portNum);
        return true;
    }

    public void installApkInit(int userNum, String path){
        paths.put(userNum,path);
    }

    public boolean installApk(int userNum){
        //install Apk in user's Device
        IChimpDevice device = devices.get(userNum);

        return device.installPackage(paths.get(userNum));
    }

    public void getPressEvent(int userId,String keyName,int flag){
        //Send PressEvent on user's Device
        IChimpDevice device = devices.get(userId);
        device.wake();

        if(flag == 0){
            device.press(keyName, TouchPressType.DOWN);
        }

        else if(flag == 1){
            device.press(keyName, TouchPressType.UP);
        }

        else if(flag == 2){
            device.press(keyName, TouchPressType.DOWN_AND_UP);
        }
    }

    public void getTouchEvent(int userId,int x,int y,int flag){
        //Send TouchEvent on user's Device
        IChimpDevice device = devices.get(userId);
        device.wake();

        if(flag == 0){
            device.touch(x, y, TouchPressType.DOWN);
        }
        else if(flag == 1){
            device.touch(x, y, TouchPressType.UP);
        }
        else if(flag == 2){
            device.touch(x, y, TouchPressType.MOVE);
        }
    }

    public String getDeviceIp(int userId){
        //get Device's ip address by userId
        Log.debugLogging(ips.toString());

        return ips.get(userId);
    }

    public String userSelectedPath(int userNum) {
        return paths.get(userNum);
    }

    public boolean isExistDevice(int userNum){
        if(devices.get(userNum) != null)
            return true;
        else
            return false;
    }

    public void disconeectDevice(int userId){
        //Disconnect Device matching userId
        System.out.println("Dispose " + userId);

        if(devices.get(userId) != null) {
            devices.get(userId).dispose();
            devices.remove(userId);
            paths.remove(userId);
        }
    }
}
