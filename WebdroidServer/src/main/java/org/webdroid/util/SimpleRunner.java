package org.webdroid.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * run process and get output and error
 * Created by micky on 2015-10-04.
 */
public class SimpleRunner {

    private ProcessBuilder processBuilder = new ProcessBuilder();

    private ArrayList<String> output = new ArrayList<>();
    private ArrayList<String> error = new ArrayList<>();

    public SimpleRunner(String... args) {
        processBuilder.command(args);
    }

    public void setWorkingDirectory(File dirFile) {
        processBuilder.directory(dirFile);
        try {
            System.out.println("set path"+dirFile.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setWorkingDirectory(String dirPath) {
        this.setWorkingDirectory(new File(dirPath));
    }

    /**
     * run process without input
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public int execute() throws IOException, InterruptedException {
        Process p = processBuilder.start();


        BufferedReader err =  new BufferedReader(new InputStreamReader(p.getErrorStream()));
        BufferedReader out = new BufferedReader(new InputStreamReader(p.getInputStream()));
        System.out.println("start");

        out.lines().forEach(s-> output.add(s));
        err.lines().forEach(s-> error.add(s));
        System.out.println("end");
        return p.waitFor();
    }

    public List<String> getOutput() {
        return output;
    }

    public List<String> getError() {
        return error;
    }

    public static void main(String[] args) {
        SimpleRunner run = new SimpleRunner("C:/Users/ksmai/Documents/androidsdk/build-tools/23.0.1/aapt", "dump", "badging", "C:\\Users\\ksmai\\Documents\\Coding\\webdroid\\WebdroidServer\\user-upload\\projects\\18_test2\\app\\build\\outputs\\apk\\app-debug.apk");
        try {
            String current = new File( "." ).getCanonicalPath();
            System.out.println("cwd:"+current);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //SimpleRunner run = new SimpleRunner("cmd", "/C", "gradlew.bat", "assemble");
        run.setWorkingDirectory("./user-upload/projects/18_Test2/");
        try {
            run.execute();
            System.out.println("output " + run.getOutput());
            System.out.println("error "+ run.getError());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Object[] activity =  run.getOutput().parallelStream().filter(s -> s.startsWith("launchable-activity")).toArray();
        System.out.println(activity[0]);
        String pased = activity[0].toString().replaceAll("launchable-activity: name=\\'", "");
        pased = pased.substring(0, pased.indexOf('\''));

        System.out.println(pased);
    }
}
