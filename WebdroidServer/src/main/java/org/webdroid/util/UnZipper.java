package org.webdroid.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by Seho on 2015-09-15.
 */
public class UnZipper {

    protected ConsoleLogger logger = ConsoleLogger.createLogger(getClass());

    public static boolean unZipper(String source, String target) {
        UnZipper unZip = new UnZipper();
        return unZip.unZipIt(source, target);
    }

    /**
     * Unzip it
     *
     * @param zipFileName      input zip file
     * @param outputFolderName zip file output folder
     */
    public boolean unZipIt(String zipFileName, String outputFolderName) {

        String zipFile = "user-upload\\projects\\" + zipFileName;
        String outputFolder = "user-upload\\projects\\" + outputFolderName;
        byte[] buffer = new byte[1024];
        ZipInputStream zis = null;
        FileOutputStream fos = null;
        boolean fos_sw = false;
        boolean zis_sw = false;

        try {

            //create output directory is not exists
            File folder = new File(outputFolder);
            if (!folder.exists()) {
                folder.mkdir();
            }

            //get the zip file content
            zis = new ZipInputStream(new FileInputStream(zipFile));
            zis_sw = true;
            logger.debug("unzip start");

            //get the zipped file list entry
            ZipEntry ze = zis.getNextEntry();
            while (ze != null) {
                if(ze.isDirectory()){
                    ze = zis.getNextEntry();
                    continue;
                }

                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);

                new File(newFile.getParent()).mkdirs();

                fos = new FileOutputStream(newFile);
                fos_sw = true;

                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }

                fos.close();
                ze = zis.getNextEntry();
            }

            zis.closeEntry();
            zis.close();

            logger.debug("unzip done");

            return true;

        } catch (IOException e) {
            logger.debug("unzip failed");
            e.printStackTrace();
            try {
                if (fos_sw) fos.close();
                if (zis_sw) {
                    zis.closeEntry();
                    zis.close();
                }
            } catch (IOException ex) {
            }
            return false;
        }
    }
}

