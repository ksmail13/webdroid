package org.webdroid.constant;

/**
 * Database queries
 * Created by micky on 2015-08-07.
 */
public class Query {
    public final static String SIGN_UP =
            "insert into user" +
            "(id, passwd, join_time, name)" +
            "value" +
            "(?,password(?),now(),?)";

    public final static String SIGN_IN =
            "select " +
            "u_id, " +
            "name, " +
            "is_enable "+
            "from user " +
            "where id=? and passwd=password(?)";

    public final static String NEW_PROJECT =
            "insert into project\n" +
            "(p_name, p_descript, p_creator, create_date)\n" +
            "value\n" +
            "(?, ?, ?, now());";

    public final static String MY_PROJECT =
            "select " +
            "p.p_id as id, " +
            "concat(p.p_name, ' <small>from ', u.name, '</small>') as name, " +
            "if(char_length(p.p_descript) > 24,  concat(substr(p.p_descript, 1,24),'...'),p.p_descript) as description, " +
            "p.p_path as path, " +
            "up.p_is_important as is_important, " +
            "p.create_date as createDate, " +
            "up.u_id as user_id, " +
            "p.p_creator " +
            "from project as p, user_project as up, user as u " +
            "where u.u_id = p.p_creator and p.p_is_working != '0' and p.p_id = up.p_id and up.u_id = ? and  " +
            "(up.p_is_important = '1' or (up.u_id = p.p_creator))";


    public final static String  NEW_GIT=
            "update user\n" +
            "set git_id=? \n"+
            "where u_id=? ;";


    public final static String  PW_CHECK=
            "SELECT count(*) as cnt\n"+
                    "FROM user \n"+
                    "where u_id=? and passwd=password(?)";


    public final static String  NEW_PW=
            "UPDATE user\n"+
            "SET passwd=password(?) \n"+
            "where u_id=?;";

    public final static String  UNSUBSCRIBE=
            "update user\n" +
            "set is_enable='0' \n"+
            "where u_id=?;";

    public final static String USER_PROFILE =

            "SELECT id, name, git_id, introduce, user_img\n" +
            "FROM user\n" +
            "WHERE u_id = ?\n";

    public final static String NEW_INTRODUCE =
            "update user " +
            "set introduce = ? " +
            "where u_id = ?";

    public final static String PROFILE_IMG_UPLOAD =
            "update user " +
            "set user_img = ? " +
            "where u_id = ?";


    public final static String SET_RANDOM_PW=
            "UPDATE user\n"+
            "SET passwd=password(?) \n"+
            "where id=?;";

    public final static String ID_CHECK=
            "SELECT count(*) as cnt\n"+
            "FROM user \n"+
            "WHERE id = ?";

    public final static String GET_PROFILE_IMAGE = "SELECT \n" +
            "user_img as path\n" +
            "from user\n" +
            "where u_id=?";


    public final static String DELETE_PROJECT =
            "update project "+
            "set p_is_working = '0', p_is_important = '0' "+
            "where p_id = ?";

    public final static String FAVORATE_PROJECT =
            "update project "+
            "set p_is_important = '1' "+
             "where p_id = ?";

    public final static String VIEW_FAVORITE_PROJECT=
            "insert into user_project (u_id, p_id, p_is_important) values (?,?,1) "+
            "on duplicate key update p_is_important=IF(p_is_important='1','0','1')";


    public final static String CANCEL_FAVORATE_PROJECT =
            "update project "+
             "set p_is_important = '0' "+
             "where p_id = ?";

    public final static String GET_PATH=
            "SELECT p_path\n"+
            "FROM project\n"+
            "WHERE p_id=?";

    public final static String SET_PATH =
            "update project " +
            "set p_path = ? " +
            "where p_id = ?";

    public final static String UPDATE_USER_PROJECT =
            "insert into user_project " +
            "(u_id,p_id) " +
            "value (?,?)";

    public final static String UPDATE_IS_ENABLE=
            "update user \n"+
            "set is_enable='1' \n"+
            "where id=?";

    public final static String GET_PROJECT_NAME ="select p_name\n" +
            "from project\n" +
            "where p_id=?";

    public final static String GET_PROJECT_INFO =/*
            "select p.p_name, up.p_is_important " +
            "from user_project as up, project as p " +
            "where p.p_id = up.p_id and up.u_id = ? and p.p_id = ?";*/
            "select p_name from project where p_id = ? " +
            "union all " +
            "select ifnull(p_is_important, count(p_is_important)) from user_project where u_id = ? and p_id=?";
}
