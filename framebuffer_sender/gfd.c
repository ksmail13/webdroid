/* Frame Getter */

#define FALSE 0
#define TRUE 1

#define JPG_QUAL 9

#define	PIXEL_UNKNOWN		 0
#define	PIXEL_RGB_565		 1			// 16-bit RGB
#define	PIXEL_RGBA_5551		 2			// 16-bit RGBA
#define	PIXEL_ARGB_4444		 3			// 16-bit ARGB

#define FB_FAIL				0
#define	VINFO_FAIL			1
#define FINFO_FAIL			2
#define MMAP_FAIL			3
#define FORMAT_UNKNOWN		4
#define FORMAT_VALUE_FAIL	5

#define CHUNK_SIZE 4096

#define SYNC_DELAY 4000

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <memory.h>
#include <jpeglib.h>

#include <linux/kd.h>
#include <linux/fb.h>

#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <asm/page.h>
#include <arpa/inet.h>

#include <math.h>

#include "FrameInfo.h"
#include "base64.h"

const char *pxFormat[4] = {"UNKNOWN", "RGB 565", "RGBA 5551", "ARGB 4444"};

static	FrameInfo frameinfo;
static struct fb_var_screeninfo vinfo;
static struct fb_fix_screeninfo	finfo;
int	fd;

int getInfo();
int getFrameBuffer (int pixelformat, unsigned char **buf);
void write_JPEG_buffer(unsigned char ** outbuffer, unsigned long * outsize, int w, int h, JSAMPLE *buf);

typedef struct buffer {
    int size;
    char buf[1024];
} Buffer;

Buffer create_int_buffer(int data) {
    Buffer buf;
    int *temp = (int *)buf.buf;
    memset(&buf, 0, sizeof(buf));
    buf.size = 4;
    *temp = data;
    return buf;
}

int main()
{
    freopen("/sdcard/log.log", "w", stdout);
	if(getInfo() >= 0)
		return -1;

	/* socket */

	int sd, csd, socklen, len, nleft = 0;
	unsigned char	*buf, *cur;
	char dummy[20];

	unsigned char * outbuffer = NULL;
	unsigned long outsize = 0;
	
	unsigned char *encodedbuffer = NULL;
	unsigned long encodedsize = 0;
	
	struct sockaddr_in server_addr;
	struct sockaddr_in client_addr;

	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sd < 0) {
		perror("Socket Error ");
		exit(0);
	}

	FILE *sfp;
	sfp = fdopen(sd, "rw");

	socklen = sizeof(server_addr);
	memset(&server_addr, 0, socklen);
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = htons(5678);

	if(bind(sd, (struct sockaddr *)&server_addr, socklen)
		== -1)
	{
		perror("Bind Error ");
		exit(0);
	}
	printf("Start Get Frame Daemon\n");

	int a = 1;

	while(a)
	{
        // 1. receive "start"
        // if receive "DIE" move to loopstart
        // wait for connecting from client
        recvfrom(sd, (void *)&dummy, sizeof(dummy), 0, (struct sockaddr *)&client_addr, &socklen);
        printf("[%s] %s\n", inet_ntoa(client_addr.sin_addr), dummy);

        if(!strcmp(dummy, "DIE"))
            continue;


        // 2. get framebuffer & jpeg compressing
		getFrameBuffer (1, &buf);
		write_JPEG_buffer(&outbuffer, &outsize, frameinfo.width, frameinfo.height, buf);
		printf("outbuffer = %x, outsize = %d\n", outbuffer, outsize);
		
		// 3. encoding to base64
		encodedsize = Base64encode_len(outsize);
		encodedbuffer = (unsigned char*)calloc(1, encodedsize);
		printf("%d -> %d\n", outsize, Base64encode(encodedbuffer, outbuffer, outsize));
		
		
		// 4. send total byte size
		nleft = ntohl(encodedsize);
		printf("outsize = %x\n",nleft);
		sendto(sd, (void *)&nleft, sizeof(int), 0, (struct sockaddr *)&client_addr, socklen);
		nleft = encodedsize;

        // 5. receive "ok"
        // if receive "DIE" move to loopstart
        recvfrom(sd, (void *)&dummy, sizeof(dummy), 0, (struct sockaddr *)&client_addr, &socklen);
        printf("[HD] %s\n", dummy);

        if(!strcmp(dummy, "DIE"))
             continue;



        cur = encodedbuffer;
		while(nleft > 0)
		{
			if(nleft > CHUNK_SIZE)
				len = sendto(sd, (void *)cur, CHUNK_SIZE,0, (struct sockaddr *)&client_addr, socklen);
			else
				len = sendto(sd, (void *)cur, nleft, 0, (struct sockaddr *)&client_addr, socklen);
	        	// delay
		        usleep(SYNC_DELAY);
			//printf("%d:send %d bytes & cur = %x\n", count++,len, cur);
			if(len == 0)
				break;
			cur += len;
			nleft -= len;
		}
		
		
		free(buf);
		fflush(sfp);
	}

	close(sd);
	free(buf);

	return 0;
}

/*
 * read framebuffer from framebuffer device file
 */
int getFrameBuffer (int pixelformat, unsigned char **buf){
	int		i,j,k;
	int		Dst;
	int		ret;

	unsigned char	*pFrame, *cur;
	int				*intbuf, *intpFrame0, *intpFrame1;
	uint16_t		*int16_buf, *int16_pFrame0, *int16_pFrame1;

	uint16_t		int16_pixel;
	unsigned char	r5, g6, b5, r8, g8, b8;

	fd = open("/dev/graphics/fb0", O_RDONLY);
	if(fd<0)
		return FB_FAIL;

	printf("[%dx%dx%d, pixelFormat= %s, fullBytes= %d]\n", frameinfo.width, frameinfo.height, frameinfo.byteperpixel*8, pxFormat[frameinfo.pixelFormat], frameinfo.fullbyte);

	pFrame = (unsigned char *)mmap(0, finfo.smem_len, PROT_READ, MAP_PRIVATE, fd,0);
	if(pFrame == MAP_FAILED){
		close(fd);
		return MMAP_FAIL;
	}
	intpFrame0	= (int *) pFrame;
	intpFrame1	= (int *) (pFrame+finfo.smem_len/2);
	int16_pFrame0 = (uint16_t *) pFrame;
	int16_pFrame1 = (uint16_t *) (pFrame+finfo.smem_len/2);

	*buf = (unsigned char *) malloc(frameinfo.fullbyte*3/2);
	cur = *buf;

	//intbuf	= (int *) *buf;
	//int16_buf	= (uint16_t *)*buf;
	
	if(frameinfo.pixelFormat == PIXEL_UNKNOWN)	return FORMAT_UNKNOWN;

	else
	if(frameinfo.pixelFormat == PIXEL_RGB_565){
		for(i=0; i < frameinfo.height ; i++){
			for(j=0; j < frameinfo.width ; j++){
				int16_pixel = *(int16_pFrame0+(i*frameinfo.width)+j);
				r5 = (int16_pixel & 0xF800) >> 11;
				g6 = (int16_pixel & 0x07E0) >> 5;
				b5 = (int16_pixel & 0x001F);
				r8 = (r5 << 3) | (r5 >> 2);
				g8 = (g6 << 2) | (g6 >> 4);
				b8 = (b5 << 3) | (b5 >> 2);
				*cur = r8; cur++;
				*cur = g8; cur++;
				*cur = b8; cur++;
			}
		}
	}

	else
	if(frameinfo.pixelFormat == PIXEL_ARGB_4444){
		for(i=0; i<frameinfo.height; i++){
			for(j=0; j<frameinfo.width; j++){
				k = *(int16_pFrame0+(i*frameinfo.width*4)+(j*2));
				*(int16_buf+(i*frameinfo.width)+j) = k;
			}
		}
	}

	else
	if(frameinfo.pixelFormat == PIXEL_RGBA_5551){
		for(i=0; i<frameinfo.height; i++){
			for(j=0; j<frameinfo.width; j++){
				k = *(intpFrame0+(i*frameinfo.width*4)+(j*2));
				Dst = k & 0xff00ff00;
				Dst += (k & 0x00ff0000) >> 16;
				Dst += (k & 0x000000ff) << 16;
				*(intbuf+(i*frameinfo.width)+j) = Dst;
			}
		}
	}

	else{
		return FORMAT_VALUE_FAIL;
	}

	//*buf = (unsigned char *) int16_buf;

	munmap(pFrame,finfo.smem_len);
	close(fd);

	return j;
}

/*
 * get screen info
 */
int getInfo(){
	int ret;

	fd = open("/dev/graphics/fb0", O_RDONLY);
	if(fd<0)
	{
		return FB_FAIL;
	}
	ret = ioctl(fd, FBIOGET_VSCREENINFO, &vinfo);
	if(ret < 0 )
	{
		close(fd);
		return VINFO_FAIL;
	}
	ret = ioctl(fd, FBIOGET_FSCREENINFO, &finfo);
	if(ret < 0 )
	{
		close(fd);
		return FINFO_FAIL;
	}

	frameinfo.width			= vinfo.xres;
	frameinfo.height		= vinfo.yres;
	frameinfo.byteperpixel	= vinfo.bits_per_pixel/8;
	frameinfo.pixelFormat	= PIXEL_RGB_565;
	frameinfo.fullbyte	= frameinfo.width * frameinfo.height * frameinfo.byteperpixel;

	return -1;
}

/*
 * raw framebuffer byte stream to compress jpeg image
 */
void write_JPEG_buffer(unsigned char ** outbuffer, unsigned long * outsize, int w, int h, JSAMPLE *buf)
{
	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;

	JSAMPROW row_pointer[1];
	int row_stride;

	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);

	jpeg_mem_dest(&cinfo, outbuffer, outsize);

	cinfo.image_width = w;
	cinfo.image_height = h;
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;


	jpeg_set_defaults(&cinfo);

	jpeg_set_quality(&cinfo, JPG_QUAL * 10, TRUE);

	jpeg_start_compress(&cinfo, TRUE);

	row_stride = w * 3;

	while(cinfo.next_scanline < cinfo.image_height) {
		row_pointer[0] = &buf[cinfo.next_scanline * row_stride];
		jpeg_write_scanlines(&cinfo, row_pointer, 1);
	}

	jpeg_finish_compress(&cinfo);

	jpeg_destroy_compress(&cinfo);
}
