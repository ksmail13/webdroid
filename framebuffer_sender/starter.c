/*
 * vm on Signal to VM Contorl Server
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <memory.h>
#include <stdlib.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>

#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>

char* getIP(int sd);
void init_dhcp();


int main(int argc, char *argv[])
{
    // logging on file
    freopen("/sdcard/starter.log", "w", stdout);
	int sd, socklen;
	char dummy[20] = {};

	struct sockaddr_in server_addr;
    sleep(5);
	sd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sd < 0) {
		perror("Socket Error ");
		exit(0);
	}

	//init_dhcp();

	socklen = sizeof(server_addr);
	memset(&server_addr, 0, socklen);
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr("121.130.206.3");
	server_addr.sin_port = htons(9990);

	strcpy(dummy, getIP(sd));
	printf("ip : %s\n", dummy);
    // signal to server
	if(sendto(sd, (void *)dummy, sizeof(dummy), 0, (struct sockaddr *)&server_addr, socklen) == -1) {
        printf("error from sendto : %s\n", strerror(errno));
    }

	close(sd);
    printf("execute gfd\n");
    fflush(stdout);
    // execute framebuffer sender
	system("/data/local/tmp/gfd");

	return 0;
}

/*
 * get current ethernet ip
 * if eth0 is doesn't work then take other ip
 */
char* getIP(int sd)
{
	struct ifreq ifr;
    char *ip = NULL;
    int res;
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name, "eth0", 5);
	res = ioctl(sd, SIOCGIFADDR, &ifr); 
    ip = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);

    if(ip[0] == '0' || res == -1) {

        ifr.ifr_addr.sa_family = AF_INET;
        strncpy(ifr.ifr_name, "eth1", 5);
        res = ioctl(sd, SIOCGIFADDR, &ifr);
        ip = inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
    }
    return ip;
}
