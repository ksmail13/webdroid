/* FrameInfo.h */

#ifndef FRAMEINFO_H_
#define FRAMEINFO_H_

#include <stdint.h>

typedef struct _FrameInfo{
	int	width;
	int	height;
	short pixelFormat;
	short byteperpixel;
	int fullbyte;
} FrameInfo;

#endif
