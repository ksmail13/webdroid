#Webdroid
---
Webdroid is a website. If you use Webdroid, you can build android source code without building Android development environments.
Also you can pre-app test without android device.
Due to these features, you can refer freely downloaded open-source to your programming. 

###Development environments and programming languages
---
- OS : (more than) windows 7
- Languages : Java 1.8, Python 2.7, gcc 4.9(Android NDK r10e)
- Tools : Intellij, pyCharm, vim, (more than) VirtualBox 4.3

###System Configuration
---
1. System architect
- website and web server instances process the request and relevant parts of the UI.
(request : log in, project management, coding)

- Sock JS processes the part connecting with the device on web pages.

- virtual device(VirtualBox) remotely run the virtual device and install compile app useing Android ADB tool. 

2. Web App
- Welcome page
  site introduce, sign in, sign up
  
- Usermain page
  profile management, project management
  
- Project view page
  code viewer, app testing, project download
  
- Setting page
  information setting, account deactivation

3. VM Server
- VM Server create and manage VM. Use VirtualBox to deal with the VM.
- VM viewer receives the display information and log data from the VM is being driven in the server and transmits inputtings like touch events that user inputs text type to VM using MonkeyRunner.
###Getting started and prerequisites
---
- OS : (more than) windows 7
- Languages : Java 1.8, Python 2.7, gcc 4.9(Android NDK r10e)
- pywin32[(http://sourceforge.net/projects/pywin32/files/pywin32/Build%20219/)](http://sourceforge.net/projects/pywin32/files/pywin32/Build%20219/)

4. Usage
- Prearrangement
  1) Add EnvSystem -> variable key:ANDROID_HOME, value:SDK installation path
							   key:path		   , value:VirtualBox installation path
  2) Edit your variable ANDROID_AAPT in WebdroidConstant.java in org.webdroid.constant package
    to let it represent your newest version of aapt.exe path
  3) 
  4) 
  4-1) Add custom screen resolution:
	VBoxManage setextradata "VM_NAME_HERE" "CustomVideoMode1" "320x480x16"
  4-2) Figure out what is the ‘hex’-value for your VideoMode:
  4-2-1) Start the VM
  4-2-2) In GRUB menu enter a
  4-2-3) In the next screen append vga=ask and press Enter
  4-2-4) Find your resolution and write down/remember the 'hex'-value for Mode column
  4-3) Translate the value to decimal notation (for example 360 hex is 864 in decimal).
  4-4) Go to menu.lst and modify it:
  4-4-1) From the GRUB menu select Debug Mode
  4-4-2) Input the following:
mount -o remount,rw /mnt  
cd /mnt/grub  
vi menu.lst
4.3. Add vga=864 (if your ‘hex’-value is 360). Now it should look like this:
kernel /android-2.3-RC1/kernel quiet root=/dev/ram0 androidboot_hardware=eeepc acpi_sleep=s3_bios,s3_mode DPI=160 UVESA_MODE=320x480 SRC=/android-2.3-RC1 SDCARD=/data/sdcard.img vga=864
4.4. Save it:
:wq


5. Unmount and reboot:

cd /
umount /mnt
reboot -f
