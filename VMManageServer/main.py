import os
from controller.ServerSocket import ServerSocket
from controller.VmController import VmController

__author__ = 'Kyle Park'


def main():
    """
    open python socket
    this socket will connect with java socket
    :return:
    """
    server_socket = ServerSocket("localhost", 1036)
    server_socket.init_socket()
    server_socket.bind_socket()
    server_socket.recv_data()
    server_socket.close_socket()

if __name__ == "__main__":
    try:
        main()
    except os.error, err:
        print "Python Server End"
