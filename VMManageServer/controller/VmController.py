import subprocess

from controller.VmSocket import VmSocket

__author__ = 'Kyle Park'


class VmController:
    """
    This class control Virtual Machine Using VirtualBox Manage Command
    """
    machineName = "android_x86"
    instance = None

    @classmethod
    def getInstance(cls):
        if cls.instance is None:
            cls.instance = VmController()
        return cls.instance

    def start_vm(self, machine_id):
        """
        Start Virtual Machine(name:android_x86_machineNum)
        if machine is already launching poweroff alread machine and restart machine
        if that machine not in server's hard disk
        make machine by call clone_machine(machineNum), set_machine_adb_port(machineNum), set_machine_framebuffer_port(machineNum)
        and start machine
        :param machine_id: from ServerSocket
        :return:
        """
        process = "VBoxManage startvm \"android_x86_" + machine_id

        pipe = subprocess.Popen(process,
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        pipe.stdin.close()

        stdout, stderr = pipe.communicate()

        if "already" in stderr:
            # Aleady Running
            print "ALREADY!"
            self.close_vm(machine_id)
            self.start_vm(machine_id)

        elif "not find" in stderr:
            # not find
            self.clone_machine(machine_id)
            self.set_machine_adb_port(machine_id)
            self.set_machine_framebuffer_port(machine_id)
            self.start_vm(machine_id)

        else:
            return "run_vm_success#" + machine_id

    def clone_machine(self, machine_id):
        """
        cloning VIrtual Machine Img android_x86 to android_x86_machine_id
        :param machine_id: from ServerSocket
        :return:
        """
        process = "VBoxManage clonevm android_x86 --name android_x86_" + machine_id + " --register"
        pipe = subprocess.Popen(process,
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        pipe.stdin.close()
        stdout, stderr = pipe.communicate()

        if "successfully cloned" in stdout :
            return True

        else :
            return False

    def set_machine_adb_port(self, machine_id):
        """
        setting machine's NAT port forwarding that communicate with adb
        :param machine_id: from ServerSocket
        :return:
        """
        port = 1110 + int(machine_id)
        process = "VBoxManage modifyvm \"android_x86_" + machine_id + "\" --natpf1 \"Rule1,tcp,," + str(
            port) + ",,5555\""

        pipe = subprocess.Popen(process,
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        pipe.stdin.close()
        pipe.communicate()

    def set_machine_framebuffer_port(self, machine_id):
        """
        setting machine's NAT port forwading that communicate with java server
        :param machine_id: from ServerSocket
        :return:
        """
        port = 1110 + int(machine_id)
        process = "VBoxManage modifyvm \"android_x86_" + machine_id + "\" --natpf1 \"Rule2,udp,," + str(
            port) + ",,5678\""

        pipe = subprocess.Popen(process,
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        pipe.stdin.close()
        stdout, stderr = pipe.communicate()

    def close_vm(self, machine_num):
        """
        power off device
        :param machine_num: from ServerSocket
        :return:
        """
        process = "VBoxManage controlvm \"android_x86_" + machine_num + "\" poweroff"
        print process
        pipe = subprocess.Popen(process,
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        pipe.stdin.close()
        stdout, stderr = pipe.communicate()

        print stdout

    def connect_vm_socket(self,vm_port):
        """
        connect Virtual Machine's starter program
        :param vm_port: 9999 port communicate with Virtual Machine's starter program
        :return: conneting Virtual Machine's Ip address
        """
        vm_socket = VmSocket(vm_port)
        vm_socket.bind_socket()
        print "VMSock bind"
        addr = vm_socket.wait_vm()
        vm_socket.close_socket()
        return addr
