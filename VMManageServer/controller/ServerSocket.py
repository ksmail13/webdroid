import thread
import socket
from controller.AdbController import AdbController
from controller.VmController import VmController

__author__ = 'Kyle Park'


def clone_vm(machine_num, client_sock) :
    """
    receive clone_vm command from java server
    make Virtual Machine by call vm_contoller.clone_vm(machine_num)
    and make port num using machine_num
    and send java server "clone_vm_success#machine_num"
    :param machine_num: receive from java server (calling user's user_id)
    :param client_sock: socket connected with java server
    :return:
    """
    vm_controller = VmController.getInstance()

    if vm_controller.clone_machine(machine_num) is False :
        client_sock.send("clone_vm_fail#" + machine_num)

    else :
        vm_controller.set_machine_adb_port(machine_num)
        vm_controller.set_machine_framebuffer_port(machine_num)

        client_sock.send("clone_vm_success#" + machine_num)


def run_vm(machine_num, client_sock) :
    """
    receive run_vm command from java server
    start Virtual Machine by call vm_contoller.start_vm(machine_num)
    and make port num using machine_num
    adb connect with starting Virtual Machine by call AdbController.connect_virtualbox(str(port))
    if adb connection succeed send java server "run_vm_success#machineNum#portNum"
    if not send java server "run_vm_fail#machineNum".

    :param machine_num: receive from java server (calling user's user_id)
    :param client_sock: socket connected with java server
    :return:
    """
    vm_controller = VmController.getInstance()
    msg = vm_controller.start_vm(machine_num)
    print "message from startVm : ", msg

    addr = vm_controller.connect_vm_socket(9999)
    print addr
    port = 1110 + int(machine_num)

    if AdbController.getInstance().connect_virtualbox(str(port)):
        client_sock.send("run_vm_success#" + machine_num + "#" + str(port))
    else:
        client_sock.send("run_vm_fail#" + machine_num)


def end_vm(machine_num, client_sock) :
    """
    receive end_vm command from java server
    power off VirtualMachine by call VmController.close_vm(machine_num)
    and send java server "end_vm_success#machine_num#"
    :param machine_num: receive from java server (calling user's user_id)
    :param client_sock: socket connected with java server
    :return:
    """
    vm_controller = VmController.getInstance()
    vm_controller.close_vm(machine_num)

    client_sock.send("end_vm_success#" + machine_num + "#")


class ServerSocket(object):
    """
    This class is socket that connect with java server's socket
    if connect is success java server will send command with user's user_id
    """
    def __init__(self, tcp_ip, tcp_port):
        """
        make tcp/ip socket that will connect with java server's socket
        :param tcp_ip: server socket's ip
        :param tcp_port: server socket's port
        :return:
        """
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientSock = None
        self.tcp_ip = tcp_ip
        self.tcp_port = tcp_port

    def init_socket(self):
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        print "init socket complete"

    def bind_socket(self):
        self.sock.bind((self.tcp_ip, self.tcp_port))
        print "bind complete"

    def recv_data(self):
        """
        receive user's command from java server's socket
        call method that suits user's command on other thread
        :return:
        """
        self.sock.listen(5)
        self.clientSock, address = self.sock.accept()
        print "accept client , address : ", address

        while True:

            print "Wait Command"
            data = self.clientSock.recv(1024)
            #command from java server's socket
            print data

            if "clone_vm" in data :
                split_data = data.split("#")

                try:
                    thread.start_new_thread(clone_vm, (split_data[1], self.clientSock))
                except:
                    print "clone_vm Thread Error!"

                continue

            if "run_vm" in data:
                #data example : run_vm#user_id
                split_data = data.split("#")

                try:
                    thread.start_new_thread(run_vm, (split_data[1], self.clientSock))
                except:
                    print "run_vm Thread Error!"

                continue

            if "end_vm" in data:
                #data example : end_vm#user_id
                split_data = data.split("#")
                try:
                    thread.start_new_thread(end_vm, (split_data[1], self.clientSock))
                except:
                    print "end_vm Thread Error!"

                continue

    def close_socket(self):
        self.sock.close()
