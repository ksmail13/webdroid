import socket

__author__ = 'Klye Park'


class VmSocket:
    """
    This Socket Class connect with Virtual Machine's starter program
    and VIrtual Machine booting complete will receive flag
    """
    def __init__(self,udp_port):
        self.udpPort = udp_port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    def bind_socket(self):
        self.sock.bind(("", self.udpPort))

    def wait_vm(self):
        """

        :return: Virtual Machine's ip address
        """
        data, (addr, port) = self.sock.recvfrom(20)

        if addr is None:
            return "connect err"

        return addr

    def close_socket(self):
        self.sock.close()
