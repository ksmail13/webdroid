import subprocess

__author__ = 'admin'


class AdbController:
    """
    Control adb command that receive user's command
    user's command : run_vm => connect_virtualbox
    user's command : end_vm => disconnet_virtualbox
    """
    adbString = "adb"
    instance = None

    @classmethod
    def getInstance(cls):
        if cls.instance is None:
            cls.instance = AdbController()
            cls.instance.kill_server()
            cls.instance.start_server()

        return cls.instance

    def start_server(self):
        """
        Starting adb's server
        :return:
        """
        pipe = subprocess.Popen(self.adbString + " start-server",
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        pipe.stdin.close()
        stdout, stderr = pipe.communicate()

        print stdout
        # return stdout, stderr


    def kill_server(self):
        """
        Starting adb's server
        :return:
        """
        pipe = subprocess.Popen(self.adbString + " kill-server",
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        pipe.stdin.close()
        stdout, stderr = pipe.communicate()

        print stdout
        # return stdout, stderr

    def connect_virtualbox(self, device_port):
        """
        adb connect with Virtual Vmachine(localhost:device_port
        :param device_port:portnumber should connection
        :return:if connect success => true, else => false
        """
        pipe = subprocess.Popen(self.adbString + " connect localhost:" + device_port,
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        pipe.stdin.close()
        stdout, stderr = pipe.communicate()

        print stdout
        print stderr

        if "connected to" in stdout:
            return True
        else:
            return False

    def disconnect_virtualbox(self, device_port):
        """
        adb disconnect with localhost:device_port
        :param device_port: portnumber should disconnection
        :return:
        """
        pipe = subprocess.Popen(self.adbString + " disconnect localhost:" + device_port,
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE)
        pipe.stdin.close()
        stdout, stderr = pipe.communicate()

        print stdout
